jutil
=====

Utility library in Java.

[![Build Status](https://travis-ci.org/KommuSoft/jutil.svg?branch=master)](https://travis-ci.org/KommuSoft/jutil)
[![Coverage Status](https://coveralls.io/repos/KommuSoft/jutil/badge.png)](https://coveralls.io/r/KommuSoft/jutil)

Testing
-------
The latest commits on each branch are always tested using `travis.ci`. Only when the branch passes all tests, it is merged into the `master` branch.

See <https://github.com/KommuSoft/jutil/branches> for a detailed report on the current state of the branches.

Coverage
--------

Javadoc
-------
