package jutils.probability;

import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

/**
 *
 * @author kommusoft
 */
public final class ProbabilityUtils {

    private static final Logger LOG = Logger.getLogger(ProbabilityUtils.class.getName());

    private static final Random RandomInstance = new Random();

    public static synchronized void setSeed(long seed) {
        RandomInstance.setSeed(seed);
    }

    public static void nextBytes(byte[] bytes) {
        RandomInstance.nextBytes(bytes);
    }

    public static int nextInt() {
        return RandomInstance.nextInt();
    }

    public static int nextInt(int n) {
        return RandomInstance.nextInt(n);
    }

    public static long nextLong() {
        return RandomInstance.nextLong();
    }

    public static boolean nextBoolean() {
        return RandomInstance.nextBoolean();
    }

    public static boolean nextBoolean(double probability) {
        return RandomInstance.nextDouble() < probability;
    }

    public static float nextFloat() {
        return RandomInstance.nextFloat();
    }

    public static double nextDouble() {
        return RandomInstance.nextDouble();
    }

    public static double[] fillRandomScale(int n) {
        double[] memory = new double[n];
        fillRandomScale(memory);
        return memory;
    }

    public static void fillRandomScale(double[] array) {
        int n = array.length;
        double invsum = 0.0d;
        for (int i = 0x00; i < n; i++) {
            double val = nextDouble();
            invsum += val;
            array[i] = val;
        }
        invsum = 1.0d / invsum;
        for (int i = 0x00; i < n; i++) {
            array[i] *= invsum;
        }
    }

    public static synchronized double nextGaussian() {
        return RandomInstance.nextGaussian();
    }

    public static double scale(double[] unscaled) {
        double sum = 0.0d;
        for (double prob : unscaled) {
            sum += prob;
        }
        double invsum = 1.0d / sum;
        for (int i = unscaled.length - 0x01; i >= 0x00; i--) {
            unscaled[i] *= invsum;
        }
        return sum;
    }

    public static <T> void shuffle(T[] data) {
        int n = data.length;
        int n1 = n - 0x01;
        T tmp;
        for (int i = 0x00; i < n1; i++) {
            tmp = data[i];
            int j = nextInt(n1 - i) + i;
            data[i] = data[j];
            data[j] = tmp;
        }
    }

    public static int getRandomIndexScaled(double... probabilities) {
        if (probabilities != null) {
            double val = nextDouble(), tmp;
            for (int i = 0x00; i < probabilities.length; i++) {
                tmp = probabilities[i];
                if (val < tmp) {
                    return i;
                } else {
                    val -= tmp;
                }
            }
        }
        return -0x01;
    }

    public static <T> void shuffle2(T[] data) {
        shuffle2(data, data.length);
    }

    public static <T> void shuffle2(T[] data, int shuffle) {
        int n = data.length;
        T tmp;
        for (int s = 0x00; s < shuffle; s++) {
            int i = nextInt(n);
            tmp = data[i];
            int j = nextInt(n);
            data[i] = data[j];
            data[j] = tmp;
        }
    }

    /**
     * Gets a random element from the given list.
     *
     * @param <T> The type of the elements of the given list.
     * @param list The given list to select an element from.
     * @return A random element from the given list.
     * @note If the list is not effective or has a size of zero, <c>null</c> is
     * returned.
     * @note If the list contains non-effective elements, <c>null</c> may be
     * returned.
     */
    public static <T> T nextElement(List<T> list) {
        if (list != null) {
            int n = list.size();
            if (n > 0x00) {
                return list.get(nextInt(n));
            }
        }
        return null;
    }

    /**
     * Gets a random element from the given list.
     *
     * @param <T> The type of the elements of the given array.
     * @param array The given array to select an element from.
     * @return A random element from the given array.
     * @note If the array is not effective or has a size of zero, <c>null</c> is
     * returned.
     * @note If the array contains non-effective elements, <c>null</c> may be
     * returned.
     */
    public static <T> T nextElement(T[] array) {
        if (array != null) {
            int n = array.length;
            if (n > 0x00) {
                return array[nextInt(n)];
            }
        }
        return null;
    }

    private ProbabilityUtils() {
    }

}
