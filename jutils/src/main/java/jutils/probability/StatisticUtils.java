package jutils.probability;

import java.util.logging.Logger;
import jutils.MathUtils;

/**
 *
 * @author kommusoft
 */
public final class StatisticUtils {

    private static final Logger LOG = Logger.getLogger(StatisticUtils.class.getName());

    private static final double[] InvP1 = {
        0.160304955844066229311E2,
        -0.90784959262960326650E2,
        0.18644914861620987391E3,
        -0.16900142734642382420E3,
        0.6545466284794487048E2,
        -0.864213011587247794E1,
        0.1760587821390590
    };

    private static final double[] InvQ1 = {
        0.147806470715138316110E2,
        -0.91374167024260313396E2,
        0.21015790486205317714E3,
        -0.22210254121855132366E3,
        0.10760453916055123830E3,
        -0.206010730328265443E2,
        0.1E1
    };

    private static final double[] InvP2 = {
        -0.152389263440726128E-1,
        0.3444556924136125216,
        -0.29344398672542478687E1,
        0.11763505705217827302E2,
        -0.22655292823101104193E2,
        0.19121334396580330163E2,
        -0.5478927619598318769E1,
        0.237516689024448000
    };

    private static final double[] InvQ2 = {
        -0.108465169602059954E-1,
        0.2610628885843078511,
        -0.24068318104393757995E1,
        0.10695129973387014469E2,
        -0.23716715521596581025E2,
        0.24640158943917284883E2,
        -0.10014376349783070835E2,
        0.1E1
    };

    private static final double[] InvP3 = {
        0.56451977709864482298E-4,
        0.53504147487893013765E-2,
        0.12969550099727352403,
        0.10426158549298266122E1,
        0.28302677901754489974E1,
        0.26255672879448072726E1,
        0.20789742630174917228E1,
        0.72718806231556811306,
        0.66816807711804989575E-1,
        -0.17791004575111759979E-1,
        0.22419563223346345828E-2
    };

    private static final double[] InvQ3 = {
        0.56451699862760651514E-4,
        0.53505587067930653953E-2,
        0.12986615416911646934,
        0.10542932232626491195E1,
        0.30379331173522206237E1,
        0.37631168536405028901E1,
        0.38782858277042011263E1,
        0.20372431817412177929E1,
        0.1E1
    };

    /**
     * Returns the inverse of the cdf of the normal distribution. Rational
     * approximations giving 16 decimals of precision. J.M. Blair, C.A. Edwards,
     * J.H. Johnson, "Rational Chebyshev approximations for the Inverse of the
     * Error Function", in Mathematics of Computation, Vol. 30, 136, pp 827,
     * (1976)
     *
     * @param u The u-score of to calculate the inverse cdf from.
     * @return The inverse of the cdf of the normal distribution.
     */
    public static double inverseF(double u) {
        int i;
        boolean negatif;
        double y, z, v, w;
        double x = u;
        if (u < 0.0 || u > 1.0) {
            throw new IllegalArgumentException("u is not in [0, 1]");
        }
        if (u <= 0.0) {
            return Double.NEGATIVE_INFINITY;
        }
        if (u >= 1.0) {
            return Double.POSITIVE_INFINITY;
        }
        x = 2.0 * x - 1.0;
        if (x < 0.0) {
            x = -x;
            negatif = true;
        } else {
            negatif = false;
        }
        if (x <= 0.75) {
            y = x * x - 0.5625;
            v = w = 0.0d;
            for (i = 6; i >= 0; i--) {
                v = v * y + InvP1[i];
                w = w * y + InvQ1[i];
            }
            z = (v / w) * x;

        } else if (x <= 0.9375) {
            y = x * x - 0.87890625;
            v = w = 0.0d;
            for (i = 7; i >= 0; i--) {
                v = v * y + InvP2[i];
                w = w * y + InvQ2[i];
            }
            z = (v / w) * x;

        } else {
            if (u > 0.5) {
                y = 1.0 / Math.sqrt(-Math.log(1.0 - x));
            } else {
                y = 1.0 / Math.sqrt(-Math.log(2.0 * u));
            }
            v = 0.0;
            for (i = 10; i >= 0; i--) {
                v = v * y + InvP3[i];
            }
            w = 0.0;
            for (i = 8; i >= 0; i--) {
                w = w * y + InvQ3[i];
            }
            z = (v / w) / y;
        }

        if (negatif) {
            if (u < 1.0e-105) {
                final double RACPI = 1.77245385090551602729;
                w = Math.exp(-z * z) / RACPI;
                y = 2.0 * z * z;
                v = 1.0;
                double term = 1.0;
                for (i = 0; i < 6; ++i) {
                    term *= -(2 * i + 1) / y;
                    v += term;
                }
                z -= u / w - 0.5 * v / z;

            }
            return -(z * MathUtils.SQRT2);

        } else {
            return z * MathUtils.SQRT2;
        }
    }

    /**
     * Computes the inverse normal distribution function with mean <SPAN
     * CLASS="MATH"><I>&#956;</I></SPAN> and variance <SPAN
     * CLASS="MATH"><I>&#963;</I><SUP>2</SUP></SPAN>. Uses different rational
     * Chebyshev approximations. Returns 16 decimal digits of precision for
     * <SPAN CLASS="MATH">2.2&#215;10<SUP>-308</SUP> &lt; <I>u</I> &lt;
     * 1</SPAN>.
     *
     * @param mu The mean to compute the inverse normal distribution from.
     * @param sigma The standard deviation to compute the inverse normal
     * @param u
     * @return
     */
    public static double inverseF(double mu, double sigma, double u) {
        return mu + sigma * inverseF(u);
    }

    private StatisticUtils() {
    }

}
