package jutils;

/**
 *
 * @author kommusoft
 */
public interface Dirtyable {

    public abstract boolean isDirty();

    public abstract void makeDirty();

}
