package jutils.maths;

/**
 * An interface specifying a matrix. A matrix is a two dimensional structure
 * it can be used to perform linear operations.
 * @author kommusoft
 */
public interface Matrix {
    
}
