package jutils.iterators;

import java.util.Iterator;
import jutils.tuples.Tuple2Base;

/**
 * An iterable that zips two iterables together in an iterable that emits tuples
 * as long as both iterables provide elements.
 *
 * @author kommusoft
 */
public class Zip2Iterable<T, Q> implements Iterable<Tuple2Base<T, Q>> {

    private final Iterable<? extends T> first;
    private final Iterable<? extends Q> second;

    /**
     * Creates a new @link{Zip2Iterable Zip2Iterable} with a given first and
     * second iterable that provide the elements.
     *
     * @param first The first iterable.
     * @param second The second iterable.
     */
    public Zip2Iterable(Iterable<? extends T> first, Iterable<? extends Q> second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public Iterator<Tuple2Base<T, Q>> iterator() {
        return new Zip2Iterator<>(this.first, this.second);
    }

    /**
     * Gets the first source iterable.
     *
     * @return the first source iterable.
     */
    protected Iterable<? extends T> getFirstIterable() {
        return first;
    }

    /**
     * Gets the second source iterable.
     *
     * @return the second source iterable.
     */
    protected Iterable<? extends Q> getSecondIterable() {
        return second;
    }
}
