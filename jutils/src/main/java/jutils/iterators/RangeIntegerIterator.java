package jutils.iterators;

import java.util.Iterator;
import java.util.logging.Logger;

/**
 *
 * @author kommusoft
 */
public class RangeIntegerIterator implements Iterator<Integer> {

    private static final Logger LOG = Logger.getLogger(RangeIntegerIterator.class.getName());

    private int from;
    private final int to;
    private final int step;

    public RangeIntegerIterator(RangeIntegerIterable rii) {
        this(rii.getFrom(), rii.getTo(), rii.getStep());
    }

    public RangeIntegerIterator(int to) {
        this(0x00, to);
    }

    public RangeIntegerIterator(int from, int to) {
        this(from, to, 0x01);
    }

    public RangeIntegerIterator(int from, int to, int step) {
        if ((to - from) / step < 0x00) {
            throw new IllegalArgumentException("RangeIntegerIterable can never reach the to value with the given step.");
        }
        this.from = from;
        this.to = to;
        this.step = step;
    }

    @Override
    public boolean hasNext() {
        return from < getTo();
    }

    @Override
    public Integer next() {
        int result = from;
        from += getStep();
        return result;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Cannot remove an element from a RangeIntegerIterator");
    }

    /**
     * @return the to
     */
    public int getTo() {
        return to;
    }

    /**
     * @return the step
     */
    public int getStep() {
        return step;
    }

}
