package jutils.iterators;

import java.util.Iterator;
import java.util.List;
import jutils.lists.ListArray;

/**
 * An iterable to iterate over a list.
 *
 * @author kommusoft
 * @param <T> The type of the list and Iterable.
 * @note The iterator is less conservative than the default iterator of the
 * list: when the list is modified, the iterator will still iterate.
 */
public class ListGenericIterable<T> implements Iterable<T> {

    private final List<? extends T> innerList;
    private final int offsetIndex;

    /**
     * Creates a new Iterable based on a list.
     *
     * @param innerList The list to iterate over.
     * @note The offset index is set to zero.
     */
    public ListGenericIterable(List<? extends T> innerList) {
        this(innerList, 0x00);
    }

    /**
     * Creates a new Iterable based on the list of elements.
     *
     * @param elements The array of elements.
     * @note The offset index is set to zero.
     */
    public ListGenericIterable(T... elements) {
        this(new ListArray<>(elements));
    }

    /**
     * Creates a new Iterable based on the list of elements.
     *
     * @param offsetIndex The offset index.
     * @param elements The array of elements.
     */
    public ListGenericIterable(int offsetIndex, T... elements) {
        this(new ListArray<>(elements), offsetIndex);
    }

    /**
     * Creates a new Iterable based on a list and offset index.
     *
     * @param innerList The list to iterate over.
     * @param offsetIndex The offset index.
     */
    public ListGenericIterable(List<? extends T> innerList, int offsetIndex) {
        this.innerList = innerList;
        this.offsetIndex = offsetIndex;
    }

    /**
     * Creates a new Iterable based on a list and offset index.
     *
     * @param offsetIndex The offset index.
     * @param innerList The list to iterate over.
     */
    public ListGenericIterable(int offsetIndex, List<? extends T> innerList) {
        this(innerList, offsetIndex);
    }

    /**
     * Creates a new iterator to iterate over the specified list.
     *
     * @return An iterator that iterates over the list.
     */
    @Override
    public Iterator<T> iterator() {
        return new ListGenericIterator<>(innerList, offsetIndex);
    }

}
