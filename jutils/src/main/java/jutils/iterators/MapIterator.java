package jutils.iterators;

import java.util.Iterator;
import java.util.logging.Logger;
import jutils.algebra.Function;
import jutils.algebra.functions.FunctionNull;

/**
 *
 * @author kommusoft
 * @param <TDomain> The type of the elements of the source iterator.
 * @param <TRange> The type of the iterated elements.
 */
public class MapIterator<TDomain, TRange> implements Iterator<TRange> {

    private static final Logger LOG = Logger.getLogger(MapIterator.class.getName());

    private final Iterator<TDomain> innerIterator;
    private final Function<TDomain, TRange> function;

    public MapIterator(Iterator<TDomain> sources, Function<TDomain, TRange> function) {
        this.innerIterator = sources;
        this.function = function;
    }

    public MapIterator(Iterable<TDomain> sources, Function<TDomain, TRange> function) {
        this(sources.iterator(), function);
    }

    public MapIterator(Function<TDomain, TRange> function, Iterable<TDomain> sources) {
        this(sources, function);
    }

    public MapIterator(Function<TDomain, TRange> function, Iterator<TDomain> sources) {
        this(sources, function);
    }

    public MapIterator(Iterable<TDomain> sources) {
        this(sources, new FunctionNull<TDomain, TRange>());
    }

    public MapIterator(Iterator<TDomain> sources) {
        this(sources, new FunctionNull<TDomain, TRange>());
    }

    @Override
    public boolean hasNext() {
        return (this.innerIterator != null && this.innerIterator.hasNext());
    }

    @Override
    public TRange next() {
        return this.function.evaluate(this.innerIterator.next());
    }

    @Override
    public void remove() {
        this.innerIterator.remove();
    }

    /**
     * @return the sources
     */
    public Iterator<TDomain> getSources() {
        return innerIterator;
    }

    /**
     * @return the function
     */
    public Function<TDomain, TRange> getFunction() {
        return function;
    }

}
