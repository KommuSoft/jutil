package jutils.iterators;

import java.util.Iterator;
import java.util.logging.Logger;

/**
 *
 * @author kommusoft
 */
public class StringIterable implements Iterable<Character> {

    private static final Logger LOG = Logger.getLogger(StringIterable.class.getName());

    private final String innerString;

    public StringIterable(String innerString) {
        this.innerString = innerString;
    }

    @Override
    public Iterator<Character> iterator() {
        return new StringIterator(this.innerString);
    }

}
