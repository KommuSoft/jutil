package jutils.iterators;

import java.util.Iterator;
import java.util.logging.Logger;

/**
 *
 * @author kommusoft
 * @param <T>
 */
public class EmptyIterable<T> implements Iterable<T> {

    public static final EmptyIterable Instance = new EmptyIterable();
    private static final Logger LOG = Logger.getLogger(EmptyIterable.class.getName());

    protected EmptyIterable() {
    }

    @Override
    @SuppressWarnings("unchecked")
    public Iterator<T> iterator() {
        return EmptyIterator.Instance;
    }

}
