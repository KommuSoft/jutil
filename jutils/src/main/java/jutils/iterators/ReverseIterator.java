package jutils.iterators;

import java.util.Iterator;
import java.util.Stack;

/**
 *
 * @author kommusoft
 * @param <T>
 */
public class ReverseIterator<T> implements Iterator<T> {

    private Iterator<T> internalIterator;
    private final Stack<T> cache = new Stack<T>();

    public ReverseIterator(Iterable<T> iterable) {
        this(iterable.iterator());
    }

    public ReverseIterator(Iterator<T> iterator) {
        this.internalIterator = iterator;
    }

    @Override
    public boolean hasNext() {
        return !cache.isEmpty() || (this.unroll());
    }

    @Override
    public T next() {
        return this.cache.pop();
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Remove not supported for a reverse iterator.");
    }

    private boolean unroll() {
        if (this.internalIterator != null) {
            while (this.internalIterator.hasNext()) {
                this.cache.push(this.internalIterator.next());
            }
            return !this.cache.isEmpty();
        } else {
            return false;
        }
    }

}
