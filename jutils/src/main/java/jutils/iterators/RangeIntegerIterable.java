package jutils.iterators;

import java.util.Iterator;
import java.util.logging.Logger;

/**
 *
 * @author kommusoft
 */
public class RangeIntegerIterable implements Iterable<Integer> {

    private static final Logger LOG = Logger.getLogger(RangeIntegerIterable.class.getName());

    private final int from;
    private final int to;
    private final int step;

    public RangeIntegerIterable(int to) {
        this(0x00, to);
    }

    public RangeIntegerIterable(int from, int to) {
        this(from, to, 0x01);
    }

    public RangeIntegerIterable(int from, int to, int step) {
        if ((to - from) / step < 0x00) {
            throw new IllegalArgumentException("RangeIntegerIterable can never reach the to value with the given step.");
        }
        this.from = from;
        this.to = to;
        this.step = step;
    }

    @Override
    public Iterator<Integer> iterator() {
        return new RangeIntegerIterator(this);
    }

    /**
     * @return the from
     */
    public int getFrom() {
        return from;
    }

    /**
     * @return the to
     */
    public int getTo() {
        return to;
    }

    /**
     * @return the step
     */
    public int getStep() {
        return step;
    }

}
