package jutils.iterators;

import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;
import jutils.lists.ListArray;

/**
 * A class to iterate over a specified list.
 *
 * @author kommusoft
 * @param <T> The type of the list and Iterator.
 * @note The iterator is less conservative than the default iterator of the
 * list: when the list is modifed, the iterator will still iterate.
 */
public class ListGenericIterator<T> implements Iterator<T> {

    private static final Logger LOG = Logger.getLogger(ListGenericIterator.class.getName());

    private final List<? extends T> innerList;
    private int index;

    /**
     * Creates a new Iterator based on a list.
     *
     * @param elements The array of elements to iterate over.
     * @note The offset index is set to zero.
     */
    public ListGenericIterator(T... elements) {
        this(new ListArray<>(elements));
    }

    /**
     * Creates a new Iterator based on a list.
     *
     * @param offsetIndex The offset index.
     * @param elements The array of elements to iterate over.
     */
    public ListGenericIterator(int offsetIndex, T... elements) {
        this(new ListArray<>(elements), offsetIndex);
    }

    /**
     * Creates a new Iterator based on a list.
     *
     * @param innerList The list to iterate over.
     * @note The offset index is set to zero.
     */
    public ListGenericIterator(List<? extends T> innerList) {
        this(innerList, 0x00);
    }

    /**
     * Creates a new Iterator based on a list and offset index.
     *
     * @param innerList The list to iterate over.
     * @param offsetIndex The offset index.
     */
    public ListGenericIterator(List<? extends T> innerList, int offsetIndex) {
        this.innerList = innerList;
        this.index = offsetIndex;
    }

    /**
     * Creates a new Iterator based on a list and offset index.
     *
     * @param offsetIndex The offset index.
     * @param innerList The list to iterate over.
     */
    public ListGenericIterator(int offsetIndex, List<? extends T> innerList) {
        this(innerList, offsetIndex);
    }

    /**
     * Checks if the the Iterator can move to the next item.
     *
     * @return True if the there is a next item in the list, false otherwise.
     */
    @Override
    public boolean hasNext() {
        return index < innerList.size();
    }

    /**
     * Returns the next element of the list. Throws an IndexOutOfBoundsException
     * if the the list is too short to move to the next item.
     *
     * @return The next element of the list.
     */
    @Override
    public T next() {
        T result = this.innerList.get(index);
        index++;
        return result;
    }

    /**
     * Removes the last returned element of the Iterator.
     *
     * @note This method only works if the specified list can be modified.
     */
    @Override
    public void remove() {
        this.innerList.remove(index - 0x01);
        index--;
    }

}
