package jutils.iterators;

import java.util.Iterator;
import jutils.tuples.Tuple2Base;

/**
 *
 * @author kommusoft
 * @param <T> The type of the first elements of the emitted tuples.
 * @param <Q> The type of the second elements of the emitted tuples.
 */
public class Zip2Iterator<T, Q> implements Iterator<Tuple2Base<T, Q>> {

    private final Iterator<? extends T> first;
    private final Iterator<? extends Q> second;

    /**
     * Creates a new instance of the @link{Zip2Iterator Zip2Iterator} with the
     * given first and second iterable.
     *
     * @param first The given first iterable.
     * @param second The given second iterable.
     */
    public Zip2Iterator(Iterable<? extends T> first, Iterable<? extends Q> second) {
        this(first.iterator(), second.iterator());
    }

    /**
     * Creates a new instance of the @link{Zip2Iterator Zip2Iterator} with a
     * given first iterable and a second iterator.
     *
     * @param first The given first iterable.
     * @param second The given second iterator.
     */
    public Zip2Iterator(Iterable<? extends T> first, Iterator<? extends Q> second) {
        this(first.iterator(), second);
    }

    /**
     * Creates a new instance of the @link{Zip2Iterator Zip2Iterator} with the
     * given first iterator and a second iterable.
     *
     * @param first The given first iterator.
     * @param second The given second iterable.
     */
    public Zip2Iterator(Iterator<? extends T> first, Iterable<? extends Q> second) {
        this(first, second.iterator());
    }

    /**
     * Creates a new instance of the @link{Zip2Iterator Zip2Iterator} with the
     * given first and second iterator.
     *
     * @param first The given first iterator.
     * @param second The given second iterator.
     */
    public Zip2Iterator(Iterator<? extends T> first, Iterator<? extends Q> second) {
        this.first = first;
        this.second = second;
    }

    /**
     * Checks if the iterator has another element to emit.
     *
     * @return True if the iterator has another element to emit, false
     * otherwise.
     */
    @Override
    public boolean hasNext() {
        return this.getFirstIterator().hasNext() && this.getSecondIterator().hasNext();
    }

    /**
     * Generates the next tuple.
     *
     * @return A tuple containing the next elements of both iterators.
     */
    @Override
    public Tuple2Base<T, Q> next() {
        return new Tuple2Base<>(this.getFirstIterator().next(), this.getSecondIterator().next());

    }

    /**
     * Removes the last emitted elements of both iterators.
     */
    @Override
    public void remove() {
        this.getFirstIterator().remove();
        this.getSecondIterator().remove();
    }

    /**
     * Gets the first iterator.
     *
     * @return the first iterator.
     */
    protected Iterator<? extends T> getFirstIterator() {
        return first;
    }

    /**
     * Gets the second iterator.
     *
     * @return the second iterator.
     */
    protected Iterator<? extends Q> getSecondIterator() {
        return second;
    }

}
