package jutils.iterators;

import java.util.Iterator;
import java.util.logging.Logger;
import jutils.lists.ListArray;

/**
 *
 * @author kommusoft
 * @param <TElement> The type of elements the iterable yields.
 */
public class AppendIterable<TElement> implements Iterable<TElement> {

    private static final Logger LOG = Logger.getLogger(AppendIterable.class.getName());

    private final Iterable<? extends Iterable<? extends TElement>> iterables;

    public AppendIterable(Iterable<? extends Iterable<? extends TElement>> iterables) {
        this.iterables = iterables;
    }

    public AppendIterable(Iterable<? extends TElement>... iterables) {
        this(new ListArray<>(iterables));
    }

    @Override
    public Iterator<TElement> iterator() {
        return new AppendIterator<>(this.iterables);
    }

}
