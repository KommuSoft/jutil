package jutils.iterators;

import java.util.Iterator;
import java.util.logging.Logger;

/**
 *
 * @author kommusoft
 * @param <TElement> The type of the element to iterate.
 * @note If the given element is not effective, no element is iterated.
 */
public class SingleIterable<TElement> implements Iterable<TElement> {

    private static final Logger LOG = Logger.getLogger(SingleIterable.class.getName());

    private final TElement element;

    public SingleIterable(TElement element) {
        this.element = element;
    }

    @Override
    public Iterator<TElement> iterator() {
        return new SingleIterator<>(element);
    }

    /**
     * @return the element
     */
    public TElement getElement() {
        return element;
    }

}
