package jutils.iterators;

import java.util.Iterator;

/**
 *
 * @author kommusoft
 * @param <TElement> The type of elements elements the iterator yields.
 */
public class AppendIterator<TElement> implements Iterator<TElement> {

    private final Iterator<? extends Iterable<? extends TElement>> iterables;
    private Iterator<? extends TElement> currentIterator = null;

    public AppendIterator(Iterator<? extends Iterable<? extends TElement>> iterables) {
        this.iterables = iterables;
    }

    public AppendIterator(Iterable<? extends Iterable<? extends TElement>> iterables) {
        this(iterables.iterator());
    }

    @Override
    public boolean hasNext() {
        this.loadNextIterable();
        return this.currentIterator.hasNext();
    }

    private void loadNextIterable() {
        if ((this.currentIterator == null || !this.currentIterator.hasNext()) && this.iterables != null) {
            while ((this.currentIterator == null || !this.currentIterator.hasNext()) && this.iterables.hasNext()) {
                this.currentIterator = this.iterables.next().iterator();
            }
        }
    }

    @Override
    public TElement next() {
        return this.currentIterator.next();
    }

    @Override
    public void remove() {
        this.currentIterator.remove();
    }

}
