package jutils.iterators;

import java.util.Iterator;
import java.util.logging.Logger;

/**
 *
 * @author kommusoft
 * @param <T>
 */
public class EmptyIterator<T> implements Iterator<T> {

    public static final EmptyIterator Instance = new EmptyIterator();
    private static final Logger LOG = Logger.getLogger(EmptyIterator.class.getName());

    protected EmptyIterator() {
    }

    @Override
    public boolean hasNext() {
        return false;
    }

    @Override
    public T next() {
        throw new UnsupportedOperationException("The end is reached.");
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Cannot remove items from an EmptyIterator.");
    }

}
