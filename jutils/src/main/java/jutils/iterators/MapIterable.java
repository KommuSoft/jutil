package jutils.iterators;

import java.util.Iterator;
import java.util.logging.Logger;
import jutils.algebra.Function;
import jutils.algebra.functions.FunctionNull;

/**
 *
 * @author kommusoft
 * @param <TDomain> The type of elements of the source iterator.
 * @param <TRange> The type of elements of the iterable.
 */
public class MapIterable<TDomain, TRange> implements Iterable<TRange> {

    private static final Logger LOG = Logger.getLogger(MapIterable.class.getName());

    private final Iterable<TDomain> innerIterable;
    private final Function<TDomain, TRange> function;

    public MapIterable(Iterable<TDomain> innerIterable, Function<TDomain, TRange> function) {
        this.innerIterable = innerIterable;
        this.function = function;
    }

    public MapIterable(Function<TDomain, TRange> function, Iterable<TDomain> innerIterable) {
        this(innerIterable, function);
    }

    public MapIterable(Iterable<TDomain> innerIterable) {
        this(innerIterable, new FunctionNull<TDomain, TRange>());
    }

    @Override
    public Iterator<TRange> iterator() {
        return new MapIterator<>(innerIterable, function);
    }

    /**
     * @return the source
     */
    public Iterable<TDomain> getSource() {
        return innerIterable;
    }

    /**
     * @return the function
     */
    public Function<TDomain, TRange> getFunction() {
        return function;
    }

}
