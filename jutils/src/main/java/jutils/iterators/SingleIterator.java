package jutils.iterators;

import java.util.Iterator;
import java.util.logging.Logger;

/**
 *
 * @author kommusoft
 * @param <TElement> The type of the element to iterate.
 * @note If the given element is not effective, no element is iterated.
 */
public class SingleIterator<TElement> implements Iterator<TElement> {

    private static final Logger LOG = Logger.getLogger(SingleIterator.class.getName());

    private TElement element;

    public SingleIterator(TElement element) {
        this.element = element;
    }

    @Override
    public boolean hasNext() {
        return (this.element != null);
    }

    @Override
    public TElement next() {
        try {
            return element;
        } finally {
            this.element = null;
        }
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("The iterator cannot modify a single element.");
    }

}
