package jutils.iterators;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.logging.Logger;

/**
 *
 * @author kommusoft
 * @param <TElement> The type of the elements of the iterable.
 */
public class CacheIterable<TElement> implements Iterable<TElement> {

    private static final Logger LOG = Logger.getLogger(CacheIterable.class.getName());

    private final LinkedList<TElement> currentCache = new LinkedList<>();
    private final Iterator<TElement> iterator;

    public CacheIterable(Iterator<TElement> iterator) {
        this.iterator = iterator;
    }

    public CacheIterable(Iterable<TElement> iterable) {
        this(iterable.iterator());
    }

    private boolean hasExtractNext() {
        return (this.iterator != null && this.iterator.hasNext());
    }

    private void extractNext() {
        currentCache.add(iterator.next());
    }

    @Override
    public Iterator<TElement> iterator() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private class CacheIterator implements Iterator<TElement> {

        private final Iterator<TElement> cacheIterator = CacheIterable.this.currentCache.iterator();

        @Override
        public boolean hasNext() {
            return (this.cacheIterator.hasNext() || CacheIterable.this.hasExtractNext());
        }

        @Override
        public TElement next() {
            if (this.cacheIterator.hasNext()) {
                return this.cacheIterator.next();
            } else {
                CacheIterable.this.extractNext();
                return this.cacheIterator.next();
            }
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("The cached iterable cannot remove items.");
        }

    }

}
