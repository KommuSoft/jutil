package jutils.iterators;

import java.util.Iterator;
import java.util.logging.Logger;

/**
 *
 * @author kommusoft
 */
public class StringIterator implements Iterator<Character> {

    private static final Logger LOG = Logger.getLogger(StringIterator.class.getName());

    private final String innerString;
    private int index = 0x00;

    public StringIterator(String innerString) {
        if (innerString != null) {
            this.innerString = innerString;
        } else {
            this.innerString = "";
        }
    }

    @Override
    public boolean hasNext() {
        return index < innerString.length();
    }

    @Override
    public Character next() {
        Character result = innerString.charAt(index);
        index++;
        return result;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Cannot remove a character with the StringIterator");
    }

}
