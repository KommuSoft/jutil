package jutils.algebra;

/**
 * The class representation of a function where an inverse function exists. An
 * inverse function maps y to x given that this function maps x to y.
 *
 * @author kommusoft
 * @param <TDomain> The domain of the function.
 * @param <TRange> The range of the function.
 * @param <TInverseFunction> The type of the inverse function.
 * @note The inverse of an inversible function does not necessary is an
 * inversible function since the inverse can be hard to compute.
 */
public interface InversibleFunction<TDomain, TRange, TInverseFunction extends Function<TRange, TDomain>> extends Function<TDomain, TRange> {

    /**
     * Returns the inverse function of this function.
     *
     * @return The inverse function of this function.
     */
    TInverseFunction getInverseFunction();

}
