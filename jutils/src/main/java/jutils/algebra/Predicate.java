package jutils.algebra;

/**
 * The class representation of a predicate. A predicate is a function that maps
 * elements from the domain to a boolean. Predicates are used to express the
 * truth about some property or element.
 *
 * @author kommusoft
 * @param <TSource> The domain of the predicate.
 */
public interface Predicate<TSource> extends Function<TSource, Boolean> {

    public abstract Predicate<TSource> getInverse();

}
