package jutils.algebra;

/**
 * The class representation of a function where the domain and the range have
 * the same type.
 *
 * @author kommusoft
 * @param <TDomainRange> The domain and the range of the function.
 */
public interface InternalFunction<TDomainRange> extends Function<TDomainRange, TDomainRange> {

}
