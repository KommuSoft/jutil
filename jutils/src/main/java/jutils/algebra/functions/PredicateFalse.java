package jutils.algebra.functions;

import jutils.algebra.Predicate;

/**
 * A predicate that always returns true.
 *
 * @author kommusoft
 * @param <TDomain> The domain of the predicate.
 */
public class PredicateFalse<TDomain> implements Predicate<TDomain> {

    /**
     * Returns the truth value of the domain element. In this case, the result
     * is always false.
     *
     * @param x The parameter to evaluate.
     * @return The truth value of the domain element. In this case, the result
     * is always false.
     */
    @Override
    public Boolean evaluate(TDomain x) {
        return false;
    }

    @Override
    public Predicate<TDomain> getInverse() {
        return new PredicateTrue<>();
    }

}
