package jutils.algebra.functions;

import java.util.logging.Logger;
import jutils.algebra.Function;

/**
 *
 * @author kommusoft
 */
public class FunctionIntegerAdd implements Function<Integer, FunctionIntegerIncrement> {

    public static final FunctionIntegerAdd Instance = new FunctionIntegerAdd();
    private static final Logger LOG = Logger.getLogger(FunctionIntegerAdd.class.getName());

    private FunctionIntegerAdd() {
    }

    @Override
    public FunctionIntegerIncrement evaluate(Integer x) {
        return new FunctionIntegerIncrement(x);
    }

}
