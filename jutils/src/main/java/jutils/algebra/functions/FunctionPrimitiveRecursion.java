package jutils.algebra.functions;

import java.util.logging.Logger;
import jutils.algebra.Function;
import jutils.tuples.Tuple2;
import jutils.tuples.Tuple2Base;

/**
 *
 * @author kommusoft
 * @param <TOther> The type of the second parameter.
 * @param <TTarget> The type of the result.
 */
public class FunctionPrimitiveRecursion<TOther, TTarget> implements Function<Tuple2<Integer, TOther>, TTarget> {

    private static final Logger LOG = Logger.getLogger(FunctionPrimitiveRecursion.class.getName());

    private final Function<TOther, TTarget> f;
    private final Function<Tuple2<Integer, TTarget>, TTarget> g;

    public FunctionPrimitiveRecursion(Function<TOther, TTarget> f, Function<Tuple2<Integer, TTarget>, TTarget> g) {
        this.f = f;
        this.g = g;
    }

    @Override
    public TTarget evaluate(Tuple2<Integer, TOther> x) {
        int n = x.getItem1();
        TTarget temp = getF().evaluate(x.getItem2());
        for (int i = 0x00; i < n; i++) {
            temp = getG().evaluate(new Tuple2Base<>(i, temp));
        }
        return temp;
    }

    /**
     * @return the f
     */
    public Function<TOther, TTarget> getF() {
        return f;
    }

    /**
     * @return the g
     */
    public Function<Tuple2<Integer, TTarget>, TTarget> getG() {
        return g;
    }

}
