package jutils.algebra.functions;

import java.util.logging.Logger;
import jutils.algebra.Function;

/**
 * A function that concatenates two functions into a new function.
 *
 * @author kommusoft
 * @param <TA> The type of the expected input.
 * @param <TB> The type of the intermediate result.
 * @param <TC> The type of the result.
 */
public class DotFunction<TA, TB, TC> implements Function<TA, TC> {

    private static final Logger LOG = Logger.getLogger(DotFunction.class.getName());

    private final Function<TA, TB> first;
    private final Function<TB, TC> second;

    /**
     * Creates a new instance of a DotFunction with a given first and second
     * function.
     *
     * @param first The first function.
     * @param second The second function.
     */
    public DotFunction(Function<TA, TB> first, Function<TB, TC> second) {
        this.first = first;
        this.second = second;
    }

    /**
     * Applies the second function on the result of the application of the first
     * function on the given value.
     *
     * @param x The given value.
     * @return The result of the second function applied on the result of the
     * first function applied on the given value.
     */
    @Override
    public TC evaluate(TA x) {
        return this.getSecondFunction().evaluate(this.getFirstFunction().evaluate(x));
    }

    /**
     * Gets the first function
     *
     * @return the first function.
     */
    protected Function<TA, TB> getFirstFunction() {
        return first;
    }

    /**
     * Gets the second function.
     *
     * @return the second function.
     */
    protected Function<TB, TC> getSecondFunction() {
        return second;
    }

}
