package jutils.algebra.functions;

import jutils.algebra.Function;

/**
 *
 * @author kommusoft
 * @param <TDomain> The domain of the function.
 * @param <TRange> The range of the function.
 */
public class FunctionNull<TDomain, TRange> implements Function<TDomain, TRange> {

    @Override
    public TRange evaluate(TDomain x) {
        return null;
    }

}
