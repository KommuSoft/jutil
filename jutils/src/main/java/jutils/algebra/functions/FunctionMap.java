package jutils.algebra.functions;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.logging.Logger;
import jutils.algebra.Function;

/**
 *
 * @author kommusoft
 * @param <TDomain> The domain of the function.
 * @param <TRange> The range of the function.
 */
public class FunctionMap<TDomain, TRange> implements Function<TDomain, TRange>, Map<TDomain, TRange> {

    private static final Logger LOG = Logger.getLogger(FunctionMap.class.getName());

    private final Map<TDomain, TRange> innerMap;

    public FunctionMap(Map<TDomain, TRange> innerMap) {
        this.innerMap = innerMap;
    }

    @Override
    public int size() {
        return this.innerMap.size();
    }

    @Override
    public boolean isEmpty() {
        return this.innerMap.isEmpty();
    }

    @Override
    public boolean containsKey(Object o) {
        return this.innerMap.containsKey(o);
    }

    @Override
    public boolean containsValue(Object o) {
        return this.innerMap.containsValue(o);
    }

    @Override
    public TRange get(Object o) {
        return this.innerMap.get(o);
    }

    @Override
    public TRange put(TDomain k, TRange v) {
        return this.innerMap.put(k, v);
    }

    @Override
    public TRange remove(Object o) {
        return this.innerMap.remove(o);
    }

    @Override
    public void putAll(Map<? extends TDomain, ? extends TRange> map) {
        getInnerMap().putAll(map);
    }

    @Override
    public void clear() {
        getInnerMap().clear();
    }

    @Override
    public Set<TDomain> keySet() {
        return this.innerMap.keySet();
    }

    @Override
    public Collection<TRange> values() {
        return this.innerMap.values();
    }

    @Override
    public Set<Entry<TDomain, TRange>> entrySet() {
        return this.innerMap.entrySet();
    }

    @Override
    public int hashCode() {
        return this.innerMap.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FunctionMap<?, ?> other = (FunctionMap<?, ?>) obj;
        return Objects.equals(this.innerMap, other.getInnerMap());
    }

    @Override
    public TRange evaluate(TDomain x) {
        return this.innerMap.get(x);
    }

    /**
     * @return the innerMap
     */
    public Map<TDomain, TRange> getInnerMap() {
        return innerMap;
    }

}
