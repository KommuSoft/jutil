/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jutils.algebra.functions;

import java.util.logging.Logger;
import jutils.algebra.Function;

/**
 * A function that maps the object to its textual representation.
 *
 * @author kommusoft
 * @param <TDomain> The type of the domain.
 */
public class FunctionToString<TDomain> implements Function<TDomain, String> {

    private static final Logger LOG = Logger.getLogger(FunctionToString.class.getName());
    public static final String NULL_STRING = "null";

    /**
     * Maps the given object to its textual representation.
     *
     * @param x The given object.
     * @return The textual representation of the object if the object is
     * effective, "null" otherwise.
     */
    @Override
    public String evaluate(TDomain x) {
        if (x != null) {
            return x.toString();
        } else {
            return NULL_STRING;
        }
    }

}
