package jutils.algebra.functions;

import java.util.logging.Logger;
import jutils.algebra.InternalFunction;
import jutils.algebra.InversibleFunction;

/**
 *
 * @author kommusoft
 */
public class FunctionDoubleIncrement implements InternalFunction<Double>, InversibleFunction<Double, Double, FunctionDoubleIncrement> {

    private static final Logger LOG = Logger.getLogger(FunctionDoubleIncrement.class.getName());

    private final double increment;

    public FunctionDoubleIncrement() {
        this(1.0d);
    }

    public FunctionDoubleIncrement(double increment) {
        this.increment = increment;
    }

    @Override
    public Double evaluate(Double x) {
        return x + increment;
    }

    /**
     * @return the increment
     */
    public double getIncrement() {
        return increment;
    }

    @Override
    public FunctionDoubleIncrement getInverseFunction() {
        return new FunctionDoubleIncrement(-this.increment);
    }

}
