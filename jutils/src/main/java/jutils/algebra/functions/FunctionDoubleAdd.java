package jutils.algebra.functions;

import jutils.algebra.Function;

/**
 *
 * @author kommusoft
 */
public class FunctionDoubleAdd implements Function<Double, FunctionDoubleIncrement> {

    @Override
    public FunctionDoubleIncrement evaluate(Double x) {
        return new FunctionDoubleIncrement(x);
    }

}
