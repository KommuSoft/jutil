package jutils.algebra.functions;

import java.util.logging.Logger;
import jutils.algebra.InternalFunction;
import jutils.algebra.InversibleFunction;

/**
 *
 * @author kommusoft
 */
public class FunctionIntegerIncrement implements InternalFunction<Integer>, InversibleFunction<Integer, Integer, FunctionIntegerIncrement> {

    private static final Logger LOG = Logger.getLogger(FunctionIntegerIncrement.class.getName());

    private final int increment;

    public FunctionIntegerIncrement() {
        this(0x01);
    }

    public FunctionIntegerIncrement(int increment) {
        this.increment = increment;
    }

    @Override
    public Integer evaluate(Integer x) {
        return x + increment;
    }

    @Override
    public FunctionIntegerIncrement getInverseFunction() {
        return new FunctionIntegerIncrement(-this.increment);
    }

}
