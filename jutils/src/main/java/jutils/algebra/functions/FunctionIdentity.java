package jutils.algebra.functions;

import jutils.algebra.InternalFunction;
import jutils.algebra.InversibleFunction;

/**
 *
 * @author kommusoft
 * @param <TDomain> The domain and range of the identity function.
 */
public class FunctionIdentity<TDomain> implements InternalFunction<TDomain>, InversibleFunction<TDomain, TDomain, FunctionIdentity<TDomain>> {

    public FunctionIdentity() {
    }

    @Override
    public TDomain evaluate(TDomain x) {
        return x;
    }

    @Override
    public FunctionIdentity<TDomain> getInverseFunction() {
        return this;
    }

}
