/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jutils.algebra.functions;

import java.util.List;
import jutils.algebra.Function;

/**
 *
 * @author kommusoft
 */
public class FunctionProjection<TRange> implements Function<List<TRange>, TRange> {

    private final int index;

    public FunctionProjection(int index) {
        this.index = index;
    }

    @Override
    public TRange evaluate(List<TRange> x) {
        return x.get(index);
    }

    /**
     * @return the index
     */
    public int getIndex() {
        return index;
    }

}
