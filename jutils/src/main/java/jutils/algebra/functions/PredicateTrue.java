package jutils.algebra.functions;

import java.util.logging.Logger;
import jutils.algebra.Predicate;

/**
 * A predicate that always returns true.
 *
 * @author kommusoft
 * @param <TDomain> The domain of the predicate.
 */
public class PredicateTrue<TDomain> implements Predicate<TDomain> {

    private static final Logger LOG = Logger.getLogger(PredicateTrue.class.getName());

    /**
     * Returns the truth value of the domain element. In this case, the result
     * is always true.
     *
     * @param x The parameter to evaluate.
     * @return The truth value of the domain element. In this case, the result
     * is always true.
     */
    @Override
    public Boolean evaluate(TDomain x) {
        return true;
    }

    @Override
    public Predicate<TDomain> getInverse() {
        return new PredicateFalse<>();
    }

}
