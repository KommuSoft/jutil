package jutils.algebra.predicates;

import jutils.algebra.Predicate;

/**
 *
 * @author kommusoft
 * @param <T> The type of the objects the predicate compares.
 */
public interface ComparingPredicate<T extends Comparable<T>> extends Predicate<T> {

    public abstract T getThreshold();

    public abstract int compareFrom(T x);

    @Override
    public abstract String toString();

}
