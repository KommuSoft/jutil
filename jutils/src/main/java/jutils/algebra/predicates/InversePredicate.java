package jutils.algebra.predicates;

import java.util.logging.Logger;
import jutils.algebra.Predicate;

/**
 *
 * @author kommusoft
 * @param <TSource>
 */
public class InversePredicate<TSource> implements Predicate<TSource> {

    private static final Logger LOG = Logger.getLogger(InversePredicate.class.getName());

    private final Predicate<TSource> original;

    public InversePredicate(Predicate<TSource> original) {
        this.original = original;
    }

    @Override
    public Predicate<TSource> getInverse() {
        return this.original;
    }

    @Override
    public Boolean evaluate(TSource x) {
        return !this.original.evaluate(x);
    }

}
