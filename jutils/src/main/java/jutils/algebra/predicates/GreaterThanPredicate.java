package jutils.algebra.predicates;

import java.util.logging.Logger;
import jutils.algebra.Predicate;

/**
 *
 * @author kommusoft
 * @param <T> The type of the objects the predicate compares.
 */
public class GreaterThanPredicate<T extends Comparable<T>> extends ComparingPredicateBase<T> {

    private static final Logger LOG = Logger.getLogger(GreaterThanPredicate.class.getName());

    public GreaterThanPredicate(T threshold) {
        super(threshold);
    }

    @Override
    protected boolean testCompareFrom(int compareResult) {
        return compareResult > 0x00;
    }

    @Override
    public String toString() {
        return String.format("> %s", this.getThreshold());
    }

    @Override
    public Predicate<T> getInverse() {
        return new LessThanOrEqualToPredicate<>(this.getThreshold());
    }

}
