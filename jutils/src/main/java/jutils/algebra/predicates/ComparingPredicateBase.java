package jutils.algebra.predicates;

/**
 *
 * @author kommusoft
 * @param <T> The type of the objects the predicate compares.
 */
public abstract class ComparingPredicateBase<T extends Comparable<T>> implements ComparingPredicate<T> {

    private final T threshold;

    protected ComparingPredicateBase(T threshold) {
        this.threshold = threshold;
    }

    @Override
    public T getThreshold() {
        return this.threshold;
    }

    @Override
    public Boolean evaluate(T x) {
        if (x != null) {
            return testCompareFrom(compareFrom(x));
        } else {
            return false;
        }
    }

    protected abstract boolean testCompareFrom(int compareResult);

    @Override
    public int compareFrom(T x) {
        return x.compareTo(threshold);
    }

}
