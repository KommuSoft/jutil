package jutils.algebra;

import jutils.algebra.functions.FunctionIntegerIncrement;

/**
 *
 * @author kommusoft
 */
public class InternalFunctionUtils {

    public static final FunctionIntegerIncrement DefaultIntegerIncrement = new FunctionIntegerIncrement();

    private InternalFunctionUtils() {
    }

}
