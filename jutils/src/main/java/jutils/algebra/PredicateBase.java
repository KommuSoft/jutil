package jutils.algebra;

import jutils.algebra.predicates.InversePredicate;

public abstract class PredicateBase<TSource> implements Predicate<TSource> {

    @Override
    public Predicate<TSource> getInverse() {
        return new InversePredicate<>(this);
    }

}
