package jutils.algebra;

/**
 * The class representation of a function. A function maps elements from the
 * Domain (depicted by <TDomain>) to elements of the Range (depicted by
 * <TRange>). Each element of the domain corresponds to only one element of the
 * range.
 *
 * @author kommusoft
 * @param <TDomain> The domain of the function.
 * @param <TRange> The range of the function.
 */
public interface Function<TDomain, TRange> {

    /**
     * Returns the element of the range that corresponds to the given element of
     * the domain.
     *
     * @param x The given element of the domain.
     * @return The corresponding element of the range.
     */
    public abstract TRange evaluate(TDomain x);

}
