package jutils;

/**
 *
 * @author kommusoft
 */
public interface Idable {

    public abstract long getId();

}
