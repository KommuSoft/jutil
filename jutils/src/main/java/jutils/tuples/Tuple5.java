package jutils.tuples;

/**
 *
 * @author kommusoft
 * @param <TItem1> The first element of the tuple.
 * @param <TItem2> The second element of the tuple.
 * @param <TItem3> The third element of the tuple.
 * @param <TItem4> The fourth element of the tuple.
 * @param <TItem5> The fifth element of the tuple.
 */
public interface Tuple5<TItem1, TItem2, TItem3, TItem4, TItem5> extends Tuple4<TItem1, TItem2, TItem3, TItem4> {

    public abstract TItem5 getItem5();

    public abstract TItem5 setItem5(TItem5 item5);

}
