package jutils.tuples;

import java.util.Objects;
import java.util.logging.Logger;

/**
 *
 * @author kommusoft
 * @param <TItem1> The first item of the tuple.
 * @param <TItem2> The second item of the tuple.
 * @param <TItem3> The third item of the tuple.
 */
public class Tuple3Base<TItem1, TItem2, TItem3> extends Tuple2Base<TItem1, TItem2> implements Tuple3<TItem1, TItem2, TItem3> {

    private static final Logger LOG = Logger.getLogger(Tuple3Base.class.getName());

    private TItem3 item3;

    public Tuple3Base() {
        super();
    }

    public Tuple3Base(TItem1 item1) {
        super(item1);
    }

    public Tuple3Base(TItem1 item1, TItem2 item2) {
        super(item1, item2);
    }

    public Tuple3Base(TItem1 item1, TItem2 item2, TItem3 item3) {
        super(item1, item2);
        this.item3 = item3;
    }

    @Override
    public TItem3 getItem3() {
        return this.item3;
    }

    @Override
    public TItem3 setItem3(TItem3 item3) {
        TItem3 temp = this.item3;
        this.item3 = item3;
        return temp;
    }

    @Override
    public int hashCode() {
        return 55447 + 89 * Objects.hashCode(this.item3) + super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Tuple3Base<?, ?, ?> other = (Tuple3Base<?, ?, ?>) obj;
        if (super.equals(other)) {
            return false;
        }
        if (!Objects.equals(this.item3, other.item3)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return String.format("< %s ; %s ; %s >", this.getItem1(), this.getItem2(), this.getItem3());
    }

}
