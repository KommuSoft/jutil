package jutils.tuples;

/**
 *
 * @author kommusoft
 * @param <T> The type of data the holder holds.
 */
public interface Holder<T> {

    public T getData();

    public T setData(T data);

    <T2 extends T> T copyFrom(Holder<T2> other);

}
