package jutils.tuples;

/**
 *
 * @author kommusoft
 * @param <TItem1> The first element of the tuple.
 * @param <TItem2> The second element of the tuple.
 * @param <TItem3> The third element of the tuple.
 * @param <TItem4> The fourth element of the tuple.
 */
public interface Tuple4<TItem1, TItem2, TItem3, TItem4> extends Tuple3<TItem1, TItem2, TItem3> {

    public abstract TItem4 getItem4();

    public abstract TItem4 setItem4(TItem4 item4);

}
