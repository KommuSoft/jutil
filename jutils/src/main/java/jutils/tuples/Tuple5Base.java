package jutils.tuples;

import java.util.Objects;
import java.util.logging.Logger;

/**
 *
 * @author kommusoft
 * @param <TItem1> The first item of the tuple.
 * @param <TItem2> The second item of the tuple.
 * @param <TItem3> The third item of the tuple.
 * @param <TItem4> The fourth item of the tuple.
 * @param <TItem5> The fifth item of the tuple.
 */
public class Tuple5Base<TItem1, TItem2, TItem3, TItem4, TItem5> extends Tuple4Base<TItem1, TItem2, TItem3, TItem4> implements Tuple5<TItem1, TItem2, TItem3, TItem4, TItem5> {

    private static final Logger LOG = Logger.getLogger(Tuple5Base.class.getName());

    private TItem5 item5;

    public Tuple5Base() {
        super();
    }

    public Tuple5Base(TItem1 item1) {
        super(item1);
    }

    public Tuple5Base(TItem1 item1, TItem2 item2) {
        super(item1, item2);
    }

    public Tuple5Base(TItem1 item1, TItem2 item2, TItem3 item3) {
        super(item1, item2, item3);
    }

    public Tuple5Base(TItem1 item1, TItem2 item2, TItem3 item3, TItem4 item4) {
        super(item1, item2, item3, item4);
    }

    public Tuple5Base(TItem1 item1, TItem2 item2, TItem3 item3, TItem4 item4, TItem5 item5) {
        super(item1, item2, item3, item4);
        this.item5 = item5;
    }

    @Override
    public TItem5 getItem5() {
        return this.item5;
    }

    @Override
    public TItem5 setItem5(TItem5 item5) {
        TItem5 temp = this.item5;
        this.item5 = item5;
        return temp;
    }

    @Override
    public int hashCode() {
        return 55447 + 89 * Objects.hashCode(this.item5) + super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Tuple5Base<?, ?, ?, ?, ?> other = (Tuple5Base<?, ?, ?, ?, ?>) obj;
        if (super.equals(other)) {
            return false;
        }
        if (!Objects.equals(this.item5, other.item5)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return String.format("< %s ; %s ; %s ; %s ; %s >", this.getItem1(), this.getItem2(), this.getItem3(), this.getItem4(), this.getItem5());
    }

}
