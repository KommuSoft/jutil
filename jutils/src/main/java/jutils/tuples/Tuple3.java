package jutils.tuples;

/**
 *
 * @author kommusoft
 * @param <TItem1> The first element of the tuple.
 * @param <TItem2> The second element of the tuple.
 * @param <TItem3> The third element of the tuple.
 */
public interface Tuple3<TItem1, TItem2, TItem3> extends Tuple2<TItem1, TItem2> {

    public abstract TItem3 getItem3();

    public abstract TItem3 setItem3(TItem3 item3);

}
