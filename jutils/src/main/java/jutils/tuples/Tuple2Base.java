package jutils.tuples;

import java.util.Objects;
import java.util.logging.Logger;

/**
 *
 * @author kommusoft
 * @param <TItem1> The first item of the tuple.
 * @param <TItem2> The second item of the tuple.
 */
public class Tuple2Base<TItem1, TItem2> extends HolderBase<TItem1> implements Tuple2<TItem1, TItem2> {

    private static final Logger LOG = Logger.getLogger(Tuple2Base.class.getName());

    private TItem2 item2;

    public Tuple2Base() {
        super();
    }

    public Tuple2Base(TItem1 item1) {
        this(item1, null);
    }

    public Tuple2Base(TItem1 item1, TItem2 item2) {
        super(item1);
        this.item2 = item2;
    }

    @Override
    public TItem1 getItem1() {
        return this.getData();
    }

    @Override
    public TItem1 setItem1(TItem1 item1) {
        return this.setData(item1);
    }

    @Override
    public TItem2 getItem2() {
        return this.item2;
    }

    @Override
    public TItem2 setItem2(TItem2 item2) {
        TItem2 temp = this.item2;
        this.item2 = item2;
        return temp;
    }

    @Override
    public <T2 extends TItem1> TItem1 copyFrom(Holder<T2> other) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int hashCode() {
        return 55447 + 89 * Objects.hashCode(this.item2) + super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Tuple2Base<?, ?> other = (Tuple2Base<?, ?>) obj;
        if (super.equals(other)) {
            return false;
        }
        if (!Objects.equals(this.item2, other.item2)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return String.format("< %s ; %s >", this.getItem1(), this.getItem2());
    }

}
