package jutils.tuples;

/**
 *
 * @author kommusoft
 * @param <TItem1> The type of the first item.
 * @param <TItem2> The type of the second item.
 */
public interface Tuple2<TItem1, TItem2> extends Holder<TItem1> {

    public abstract TItem1 getItem1();

    public abstract TItem1 setItem1(TItem1 item1);

    public abstract TItem2 getItem2();

    public abstract TItem2 setItem2(TItem2 item2);

}
