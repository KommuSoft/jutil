package jutils.tuples.functions;

import jutils.algebra.Function;
import jutils.tuples.Holder;

/**
 * A function that converts the first element of the tuple.
 *
 * @author kommusoft
 * @param <T> The type of the first parameter.
 */
public class TupleFirstFunction<T> implements Function<Holder<T>, T> {

    /**
     * A static instance of the function. This instance reduces the memory
     * footprint.
     */
    public static final TupleFirstFunction Instance = new TupleFirstFunction();

    /**
     * A protected constructor to create a new instance.
     *
     * @note The constructor is protected to prevent new instances to generate
     * and thus reduce the memory footprint.
     */
    protected TupleFirstFunction() {
    }

    /**
     * Gets the first element of the given tuple.
     *
     * @param x The tuple to fetch the element from.
     * @return The first element of the given tuple.
     */
    @Override
    public T evaluate(Holder<T> x) {
        if (x != null) {
            return x.getData();
        } else {
            return null;
        }
    }

}
