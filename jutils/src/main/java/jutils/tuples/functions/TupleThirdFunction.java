package jutils.tuples.functions;

import jutils.algebra.Function;
import jutils.tuples.Tuple3;

/**
 * A function that converts the third element of the tuple.
 *
 * @author kommusoft
 * @param <T> The type of the third parameter.
 */
public class TupleThirdFunction<T> implements Function<Tuple3<?, ?, T>, T> {

    /**
     * A static instance of the function. This instance reduces the memory
     * footprint.
     */
    public static final TupleThirdFunction Instance = new TupleThirdFunction();

    /**
     * A protected constructor to create a new instance.
     *
     * @note The constructor is protected to prevent new instances to generate
     * and thus reduce the memory footprint.
     */
    protected TupleThirdFunction() {
    }

    /**
     * Gets the third element of the given tuple.
     *
     * @param x The tuple to fetch the element from.
     * @return The third element of the given tuple.
     */
    @Override
    public T evaluate(Tuple3<?, ?, T> x) {
        if (x != null) {
            return x.getItem3();
        } else {
            return null;
        }
    }

}
