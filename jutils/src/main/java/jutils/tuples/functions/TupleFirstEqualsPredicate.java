package jutils.tuples.functions;

import java.util.Objects;
import java.util.logging.Logger;
import jutils.algebra.PredicateBase;
import jutils.tuples.Holder;

/**
 * A predicate that checks if the first element of the tuple is equal to the
 * first element of the tuple.
 *
 * @author kommusoft
 * @param <T> The type of the first element.
 */
public class TupleFirstEqualsPredicate<T> extends PredicateBase<Holder<T>> {

    private static final Logger LOG = Logger.getLogger(TupleFirstEqualsPredicate.class.getName());

    private final T other;

    /**
     * Creates a new instance of a @link{TupleFirstEqualsPredicate
     * TupleFirstEqualsPredicate} with a given parameter to compare.
     *
     * @param other The given parameter to compare with.
     */
    public TupleFirstEqualsPredicate(T other) {
        this.other = other;
    }

    /**
     * Checks if the first element of the given tuple is equal to the stored
     * parameter.
     *
     * @param x The given tuple to check.
     * @return True if the first element of the given tuple is equal to the
     * given parameter, false otherwise.
     */
    @Override
    public Boolean evaluate(Holder<T> x) {
        if (x != null) {
            return Objects.equals(this.other, x.getData());
        } else {
            return false;
        }
    }

}
