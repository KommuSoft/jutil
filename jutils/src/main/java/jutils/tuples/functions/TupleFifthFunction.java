package jutils.tuples.functions;

import jutils.algebra.Function;
import jutils.tuples.Tuple5;

/**
 * A function that converts the fifth element of the tuple.
 *
 * @author kommusoft
 * @param <T> The type of the fifth parameter.
 */
public class TupleFifthFunction<T> implements Function<Tuple5<?, ?, ?, ?, T>, T> {

    /**
     * A static instance of the function. This instance reduces the memory
     * footprint.
     */
    public static final TupleFifthFunction Instance = new TupleFifthFunction();

    /**
     * A protected constructor to create a new instance.
     *
     * @note The constructor is protected to prevent new instances to generate
     * and thus reduce the memory footprint.
     */
    protected TupleFifthFunction() {
    }

    /**
     * Gets the fifth element of the given tuple.
     *
     * @param x The tuple to fetch the element from.
     * @return The fifth element of the given tuple.
     */
    @Override
    public T evaluate(Tuple5<?, ?, ?, ?, T> x) {
        if (x != null) {
            return x.getItem5();
        } else {
            return null;
        }
    }

}
