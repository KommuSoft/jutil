package jutils.tuples.functions;

import jutils.algebra.Function;
import jutils.tuples.Tuple2;

/**
 * A function that converts the second element of the tuple.
 *
 * @author kommusoft
 * @param <T> The type of the second parameter.
 */
public class TupleSecondFunction<T> implements Function<Tuple2<?, T>, T> {

    /**
     * A static instance of the function. This instance reduces the memory
     * footprint.
     */
    public static final TupleSecondFunction Instance = new TupleSecondFunction();

    /**
     * A protected constructor to create a new instance.
     *
     * @note The constructor is protected to prevent new instances to generate
     * and thus reduce the memory footprint.
     */
    protected TupleSecondFunction() {
    }

    /**
     * Gets the second element of the given tuple.
     *
     * @param x The tuple to fetch the element from.
     * @return The second element of the given tuple.
     */
    @Override
    public T evaluate(Tuple2<?, T> x) {
        if (x != null) {
            return x.getItem2();
        } else {
            return null;
        }
    }

}
