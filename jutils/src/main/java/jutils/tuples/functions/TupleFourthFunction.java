package jutils.tuples.functions;

import jutils.algebra.Function;
import jutils.tuples.Tuple4;

/**
 * A function that converts the fourth element of the tuple.
 *
 * @author kommusoft
 * @param <T> The type of the fourth parameter.
 */
public class TupleFourthFunction<T> implements Function<Tuple4<?, ?, ?, T>, T> {

    /**
     * A static instance of the function. This instance reduces the memory
     * footprint.
     */
    public static final TupleFourthFunction Instance = new TupleFourthFunction();

    /**
     * A protected constructor to create a new instance.
     *
     * @note The constructor is protected to prevent new instances to generate
     * and thus reduce the memory footprint.
     */
    protected TupleFourthFunction() {
    }

    /**
     * Gets the fourth element of the given tuple.
     *
     * @param x The tuple to fetch the element from.
     * @return The fourth element of the given tuple.
     */
    @Override
    public T evaluate(Tuple4<?, ?, ?, T> x) {
        if (x != null) {
            return x.getItem4();
        } else {
            return null;
        }
    }

}
