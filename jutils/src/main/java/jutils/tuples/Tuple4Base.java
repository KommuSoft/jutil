package jutils.tuples;

import java.util.Objects;
import java.util.logging.Logger;

/**
 *
 * @author kommusoft
 * @param <TItem1> The first item of the tuple.
 * @param <TItem2> The second item of the tuple.
 * @param <TItem3> The third item of the tuple.
 * @param <TItem4> The fourth item of the tuple.
 */
public class Tuple4Base<TItem1, TItem2, TItem3, TItem4> extends Tuple3Base<TItem1, TItem2, TItem3> implements Tuple4<TItem1, TItem2, TItem3, TItem4> {

    private static final Logger LOG = Logger.getLogger(Tuple4Base.class.getName());

    private TItem4 item4;

    public Tuple4Base() {
        super();
    }

    public Tuple4Base(TItem1 item1) {
        super(item1);
    }

    public Tuple4Base(TItem1 item1, TItem2 item2) {
        super(item1, item2);
    }

    public Tuple4Base(TItem1 item1, TItem2 item2, TItem3 item3) {
        super(item1, item2, item3);
    }

    public Tuple4Base(TItem1 item1, TItem2 item2, TItem3 item3, TItem4 item4) {
        super(item1, item2, item3);
        this.item4 = item4;
    }

    @Override
    public TItem4 getItem4() {
        return this.item4;
    }

    @Override
    public TItem4 setItem4(TItem4 item4) {
        TItem4 temp = this.item4;
        this.item4 = item4;
        return temp;
    }

    @Override
    public int hashCode() {
        return 55447 + 89 * Objects.hashCode(this.item4) + super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Tuple4Base<?, ?, ?, ?> other = (Tuple4Base<?, ?, ?, ?>) obj;
        if (super.equals(other)) {
            return false;
        }
        if (!Objects.equals(this.item4, other.item4)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return String.format("< %s ; %s ; %s ; %s >", this.getItem1(), this.getItem2(), this.getItem3(), this.getItem4());
    }

}
