package jutils.testing;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Stack;
import java.util.logging.Logger;
import junit.framework.Assert;
import static junit.framework.Assert.fail;
import jutils.algebra.Predicate;

/**
 *
 * @author kommusoft
 */
public class AssertExtensions {

    public static final double INITIAL_EPSILON = 1e-06d;
    private static final Stack<Double> epsilonStack = new Stack<>();
    private static final Logger LOG = Logger.getLogger(AssertExtensions.class.getName());

    private static <T> Iterator<T> getIterator(Iterable<T> iterable) {
        if (iterable != null) {
            return iterable.iterator();
        } else {
            return null;
        }
    }

    private static <T> Iterator<T> getIterator(Iterator<T> iterator) {
        return iterator;
    }

    private static Iterator<?> getIterator(Object potentialIterator) {
        if (potentialIterator instanceof Iterator<?>) {
            return (Iterator<?>) potentialIterator;
        } else if (potentialIterator instanceof Iterable<?>) {
            return getIterator((Iterable<?>) potentialIterator);
        } else {
            return null;
        }
    }

    /**
     * @return the EPSILON
     */
    public static double getEpsilon() {
        if (!epsilonStack.isEmpty()) {
            return epsilonStack.peek();
        } else {
            return INITIAL_EPSILON;
        }
    }

    /**
     * @param epsilon the epsilon to set
     */
    public static void setEpsilon(double epsilon) {
        epsilonStack.set(0x00, epsilon);
    }

    public static void assertEquals(double expected, double result, double delta) {
        Assert.assertEquals(expected, result, delta);
    }

    public static void assertEquals(double expected, double result) {
        Assert.assertEquals(expected, result, getEpsilon());
    }

    public static void assertEquals(String message, double expected, double result, double delta) {
        Assert.assertEquals(message, expected, result, delta);
    }

    public static void assertEquals(String message, double expected, double result) {
        Assert.assertEquals(message, expected, result, getEpsilon());
    }

    public static <T> void assertTypeof(Class<T> expectedType, Object object) {
        if (expectedType != null && !expectedType.isInstance(object)) {
            Class<?> realType = null;
            if (object != null) {
                realType = object.getClass();
            }
            fail(String.format("Expected the object to be of type %s, but was %s.", expectedType, realType));
        }
    }

    public static <T> void assertTypeof(String message, Class<T> expectedType, Object object) {
        if (expectedType != null && !expectedType.isInstance(object)) {
            fail(message);
        }
    }

    public static <T> void exists(Iterable<T> iterable, Predicate<T> predicate) {
        if (iterable != null) {
            exists(iterable.iterator(), predicate);
        } else {
            fail("Iterable was null, and therefore contains no elements");
        }
    }

    public static <T> void exists(Iterator<T> iterator, Predicate<T> predicate) {
        if (iterator == null) {
            fail("Iterator was null, and therefore contains no elements");
        } else if (predicate == null) {
            fail("Predicate was null.");
        } else {
            while (iterator.hasNext()) {
                T val = iterator.next();
                if (predicate.evaluate(val)) {
                    return;
                }
            }
            fail("None of the elements satisfies the given predicate.");
        }
    }

    public static <T> void exists1(Iterable<T> iterable, Predicate<T> predicate) {
        existsN(iterable, predicate, 1);
    }

    public static <T> void exists1(Iterator<T> iterator, Predicate<T> predicate) {
        existsN(iterator, predicate, 1);
    }

    public static <T> void existsN(Iterable<T> iterable, Predicate<T> predicate, int n) {
        if (iterable != null) {
            existsN(iterable.iterator(), predicate, n);
        } else {
            fail("Iterable was null, and therefore contains no elements");
        }
    }

    public static <T> void existsN(Iterator<T> iterator, Predicate<T> predicate, int n) {
        if (iterator == null) {
            fail("Iterator was null, and therefore contains no elements");
        } else if (predicate == null) {
            fail("Predicate was null.");
        } else {
            int counter = 0x00;
            while (iterator.hasNext()) {
                T val = iterator.next();
                if (predicate.evaluate(val)) {
                    counter++;
                    if (counter > n) {
                        fail("Too much elements satisfy the given constraint.");
                    }
                }
            }
            if (counter < n) {
                fail("Too few of the elements satisfies the given predicate.");
            }
        }
    }

    public static <T> void all(Iterable<T> iterable, Predicate<T> predicate) {
        if (iterable != null) {
            all(iterable.iterator(), predicate);
        }
    }

    public static <T> void all(Iterator<T> iterator, Predicate<T> predicate) {
        if (iterator != null) {
            if (predicate == null) {
                fail("Predicate was null.");
            } else {
                while (iterator.hasNext()) {
                    T val = iterator.next();
                    if (!predicate.evaluate(val)) {
                        fail(String.format("Elements \"%s\" dit not satisfy the given predicate.", val));
                    }
                }
            }
        }
    }

    public static <T1, T2> void assertEqualsUnordered(Iterable<T1> iterable1, Iterable<T2> iterable2) {
        if (iterable1 != null && iterable2 != null) {
            assertEqualsUnordered(iterable1.iterator(), iterable2.iterator());

        } else if ((iterable1 != null) != (iterable2 != null)) {
            fail("One of the iterables is not effective while the other is.");
        }
    }

    public static <T1, T2> void assertEqualsUnordered(Iterator<T1> iterator1, Iterable<T2> iterable2) {
        if (iterator1 != null && iterable2 != null) {
            assertEqualsUnordered(iterator1, iterable2.iterator());

        } else if ((iterator1 != null) != (iterable2 != null)) {
            fail("One of the iterables is not effective while the other is.");
        }
    }

    public static <T1, T2> void assertEqualsUnordered(Iterable<T1> iterable1, Iterator<T2> iterator2) {
        if (iterable1 != null && iterator2 != null) {
            assertEqualsUnordered(iterable1.iterator(), iterator2);

        } else if ((iterable1 != null) != (iterator2 != null)) {
            fail("One of the iterables is not effective while the other is.");
        }
    }

    public static <T1, T2> void assertEqualsUnordered(Iterator<T1> iterator1, Iterator<T2> iterator2) {
        if (iterator1 != null && iterator2 != null) {
            boolean nxt1 = iterator1.hasNext(), nxt2 = iterator2.hasNext();
            HashSet<T1> col1 = new HashSet<>();
            HashSet<T2> col2 = new HashSet<>();
            while (nxt1) {
                col1.add(iterator1.next());
                nxt1 = iterator1.hasNext();
            }
            while (nxt2) {
                T2 val2 = iterator2.next();
                if (!col1.contains(val2)) {
                    fail(String.format("The first iterator does not contain %s.", val2));
                }
                col2.add(val2);
                nxt2 = iterator2.hasNext();
            }
            for (T1 val1 : col1) {
                if (!col2.contains(val1)) {
                    fail(String.format("The second iterator does not contain %s.", val1));
                }
            }

        } else if ((iterator1 != null) != (iterator2 != null)) {
            fail("One of the iterators is not effective while the other is.");
        }
    }

    public static <T1, T2> void assertEqualsOrdered(Iterable<T1> iterable1, Iterable<T2> iterable2) {
        if (iterable1 != null && iterable2 != null) {
            assertEqualsOrdered(iterable1.iterator(), iterable2.iterator());

        } else if ((iterable1 != null) != (iterable2 != null)) {
            fail("One of the iterables is not effective while the other is.");
        }
    }

    public static <T1, T2> void assertEqualsOrdered(Iterator<T1> iterator1, Iterable<T2> iterable2) {
        if (iterator1 != null && iterable2 != null) {
            assertEqualsOrdered(iterator1, iterable2.iterator());

        } else if ((iterator1 != null) != (iterable2 != null)) {
            fail("One of the iterables is not effective while the other is.");
        }
    }

    public static <T1, T2> void assertEqualsOrdered(Iterable<T1> iterable1, Iterator<T2> iterator2) {
        if (iterable1 != null && iterator2 != null) {
            assertEqualsOrdered(iterable1.iterator(), iterator2);

        } else if ((iterable1 != null) != (iterator2 != null)) {
            fail("One of the iterables is not effective while the other is.");
        }
    }

    public static <T1, T2> void assertEqualsOrdered(Iterator<T1> iterator1, Iterator<T2> iterator2) {
        if (iterator1 != null && iterator2 != null) {
            boolean nxt1 = iterator1.hasNext(), nxt2 = iterator2.hasNext();
            while (nxt1 && nxt2) {
                T1 val1 = iterator1.next();
                T2 val2 = iterator2.next();
                if (!Objects.equals(val1, val2)) {
                    fail(String.format("The first iterator iterates %s while the second iterates %s", val1, val2));
                }
                nxt1 = iterator1.hasNext();
                nxt2 = iterator2.hasNext();
            }
            if (nxt1 != nxt2) {
                fail("One of the iterators is longer than the other one.");
            }

        } else if ((iterator1 != null) != (iterator2 != null)) {
            fail("One of the iterators is not effective while the other is.");
        }
    }

    public static <T1, T2> void assertEqualsOrderedDeep(Iterable<T1> iterable1, Iterable<T2> iterable2) {
        if (iterable1 != null && iterable2 != null) {
            assertEqualsOrderedDeep(iterable1.iterator(), iterable2.iterator());

        } else if ((iterable1 != null) != (iterable2 != null)) {
            fail("One of the iterables is not effective while the other is.");
        }
    }

    public static <T1, T2> void assertEqualsOrderedDeep(Iterator<T1> iterator1, Iterable<T2> iterable2) {
        if (iterator1 != null && iterable2 != null) {
            assertEqualsOrderedDeep(iterator1, iterable2.iterator());

        } else if ((iterator1 != null) != (iterable2 != null)) {
            fail("One of the iterables is not effective while the other is.");
        }
    }

    public static <T1, T2> void assertEqualsOrderedDeep(Iterable<T1> iterable1, Iterator<T2> iterator2) {
        if (iterable1 != null && iterator2 != null) {
            assertEqualsOrderedDeep(iterable1.iterator(), iterator2);

        } else if ((iterable1 != null) != (iterator2 != null)) {
            fail("One of the iterables is not effective while the other is.");
        }
    }

    public static <T1, T2> void assertEqualsOrderedDeep(Iterator<T1> iterator1, Iterator<T2> iterator2) {
        if (iterator1 != null && iterator2 != null) {
            boolean nxt1 = iterator1.hasNext(), nxt2 = iterator2.hasNext();
            while (nxt1 && nxt2) {
                T1 val1 = iterator1.next();
                T2 val2 = iterator2.next();
                Iterator<?> it1 = getIterator(val1);
                Iterator<?> it2 = getIterator(val2);
                if (it1 != null && it2 != null) {
                    assertEqualsOrderedDeep(it1, it2);
                } else if (!Objects.equals(val1, val2)) {
                    fail(String.format("The first iterator iterates %s while the second iterates %s", val1, val2));
                }
                nxt1 = iterator1.hasNext();
                nxt2 = iterator2.hasNext();
            }
            if (nxt1 != nxt2) {
                fail("One of the iterators is longer than the other one.");
            }

        } else if ((iterator1 != null) != (iterator2 != null)) {
            fail("One of the iterators is not effective while the other is.");
        }
    }

    public static <TKey, TValue> void assertEquals(final Map<TKey, TValue> expected, final Map<TKey, TValue> result) {
        if (expected != null && result != null) {
            for (Entry<TKey, TValue> entry : expected.entrySet()) {
                if (result.containsKey(entry.getKey())) {
                    Assert.assertEquals(String.format("The values of %s do not correspond.", entry.getKey()), entry.getValue(), result.get(entry.getKey()));
                } else {
                    fail(String.format("The result does not contain an entry for %s.", entry.getKey()));
                }
            }
            for (Entry<TKey, TValue> entry : result.entrySet()) {
                if (expected.containsKey(entry.getKey())) {
                    Assert.assertEquals(String.format("The values of %s do not correspond.", entry.getKey()), expected.get(entry.getKey()), entry.getValue());
                } else {
                    fail(String.format("The result contains an entry for %s, while this is not expected.", entry.getKey()));
                }
            }
        } else if ((expected == null) != (result == null)) {
            fail("Only one of the two maps is effective.");
        }
    }

    public static <TKey, TValue> void assertEquals(String message, final Map<TKey, TValue> expected, final Map<TKey, TValue> result) {
        if (expected != null && result != null) {
            for (Entry<TKey, TValue> entry : expected.entrySet()) {
                if (result.containsKey(entry.getKey())) {
                    Assert.assertEquals(message, entry.getValue(), result.get(entry.getKey()));
                } else {
                    fail(message);
                }
            }
            for (Entry<TKey, TValue> entry : result.entrySet()) {
                if (expected.containsKey(entry.getKey())) {
                    Assert.assertEquals(message, expected.get(entry.getKey()), entry.getValue());
                } else {
                    fail(message);
                }
            }
        } else if ((expected == null) != (result == null)) {
            fail(message);
        }
    }

    public static void pushEpsilon(double epsilon) {
        epsilonStack.push(epsilon);
    }

    public static double popEpsilon() {
        if (!epsilonStack.isEmpty()) {
            return epsilonStack.pop();
        } else {
            return INITIAL_EPSILON;
        }
    }

    public static void assertLessThan(double value, double threshold) {
        if (value >= threshold) {
            fail(String.format("Expected the result to be less than %s but was %s.", threshold, value));
        }
    }

    public static void assertLessThanOrEqual(double value, double threshold) {
        if (value > threshold) {
            fail(String.format("Expected the result to be less than or equl to %s but was %s.", threshold, value));
        }
    }

    public static void assertGreaterThan(double value, double threshold) {
        if (value <= threshold) {
            fail(String.format("Expected the result to be greater than %s but was %s.", threshold, value));
        }
    }

    public static void assertGreaterThanOrEqual(double value, double threshold) {
        if (value < threshold) {
            fail(String.format("Expected the result to be greater than or equal to %s but was %s.", threshold, value));
        }
    }

    public static void assertLessThan(int value, int threshold) {
        if (value >= threshold) {
            fail(String.format("Expected the result to be less than %s but was %s.", threshold, value));
        }
    }

    public static void assertLessThanOrEqual(int value, int threshold) {
        if (value > threshold) {
            fail(String.format("Expected the result to be less than or equl to %s but was %s.", threshold, value));
        }
    }

    public static void assertGreaterThan(int value, int threshold) {
        if (value <= threshold) {
            fail(String.format("Expected the result to be greater than %s but was %s.", threshold, value));
        }
    }

    public static void assertGreaterThanOrEqual(int value, int threshold) {
        if (value < threshold) {
            fail(String.format("Expected the result to be greater than or equal to %s but was %s.", threshold, value));
        }
    }

    public static <T extends Comparable<T>> void assertLessThan(T value, T threshold) {
        if (value.compareTo(threshold) >= 0x00) {
            fail(String.format("Expected the result to be less than %s but was %s.", threshold, value));
        }
    }

    public static <T extends Comparable<T>> void assertLessThanOrEqual(T value, T threshold) {
        if (value.compareTo(threshold) > 0x00) {
            fail(String.format("Expected the result to be less than or equl to %s but was %s.", threshold, value));
        }
    }

    public static <T extends Comparable<T>> void assertGreaterThan(T value, T threshold) {
        if (value.compareTo(threshold) <= 0x00) {
            fail(String.format("Expected the result to be greater than %s but was %s.", threshold, value));
        }
    }

    public static <T extends Comparable<T>> void assertGreaterThanOrEqual(T value, T threshold) {
        if (value.compareTo(threshold) < 0x00) {
            fail(String.format("Expected the result to be greater than or equal to %s but was %s.", threshold, value));
        }
    }

    public static <T> void assertContains(T element, Collection<T> collection) {
        if (collection == null) {
            fail("Collection is not effective.");
        } else if (!collection.contains(element)) {
            fail(String.format("Collection %s does not contain the element %s.", collection, element));
        }
    }

    public static <T> void assertContains(T element, Iterable<T> iterable) {
        if (iterable == null) {
            fail("Iterable is not effective.");
        } else {
            assertContains(element, iterable.iterator());
        }
    }

    @SuppressWarnings("empty-statement")
    public static <T> void assertContains(T element, Iterator<T> iterator) {
        if (iterator == null) {
            fail("Iterator is not effective.");
        } else {
            while (iterator.hasNext() && !Objects.equals(iterator.next(), element));
            fail(String.format("Iterator %s does not contain the element %s.", iterator, element));
        }
    }

    private AssertExtensions() {
    }

}
