package jutils;

import java.util.logging.Logger;

/**
 *
 * @author kommusoft
 */
public class MathUtils {

    public static final double LOG2 = Math.log(2.0d);
    public static final double SQRT2 = Math.sqrt(2.0d);
    public static final double INVLOG2 = 1.0d / LOG2;
    public static final double PI = Math.PI;
    public static final double THETA = 0.5d * Math.PI;
    public static final double E = Math.E;
    private static final Logger LOGGER = Logger.getLogger(MathUtils.class.getName());

    public static double log(double a, double x) {
        return Math.log(x) / Math.log(a);
    }

    public static double log(double x) {
        return Math.log(x);
    }

    public static double log2(double x) {
        return Math.log(x) * INVLOG2;
    }

    private MathUtils() {
    }

}
