package jutils.lists;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;
import java.util.logging.Logger;
import jutils.iterators.ListGenericIterator;

/**
 *
 * @author kommusoft
 * @param <T>
 */
public class ListArray<T> implements List<T> {

    private static final Logger LOG = Logger.getLogger(ListArray.class.getName());

    private final int from, to;
    private final T[] data;

    public ListArray(T... data) {
        this(0x00, data.length, data);
    }

    public ListArray(int from, int to, T... data) {
        this.from = Math.max(0x00, Math.min(from, data.length - 0x01));
        this.to = Math.max(0x00, Math.min(to, data.length));
        this.data = data;
    }

    @Override
    public int size() {
        return to - from;
    }

    @Override
    public boolean isEmpty() {
        return to <= from;
    }

    @Override
    public boolean contains(Object o) {
        for (int i = from; i < to; i++) {
            if (Objects.equals(data[i], o)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return new ListGenericIterator<>(this);
    }

    @Override
    public Object[] toArray() {
        int n = this.to - this.from;
        Object[] result = new Object[n];
        System.arraycopy(this.data, this.from, result, 0, n);
        return result;
    }

    @Override
    public <T> T[] toArray(T[] ts) {
        int n = this.to - this.from;
        @SuppressWarnings("unchecked")
        T[] result = (T[]) Array.newInstance(ts.getClass().getComponentType(), n);
        System.arraycopy(this.data, this.from, result, 0, n);
        return result;
    }

    @Override
    public boolean add(T e) {
        throw new UnsupportedOperationException("Cannot add elements to a fixed array.");
    }

    @Override
    public boolean remove(Object o) {
        throw new UnsupportedOperationException("Cannot remove elements from a fixed array.");
    }

    @Override
    public boolean containsAll(Collection<?> clctn) {
        for (Object o : clctn) {
            if (!this.contains(o)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> clctn) {
        if (clctn.size() > 0x00) {
            throw new UnsupportedOperationException("Cannot add elements to a fixed array.");
        } else {
            return false;
        }
    }

    @Override
    public boolean addAll(int i, Collection<? extends T> clctn) {
        if (clctn.size() > 0x00) {
            throw new UnsupportedOperationException("Cannot add elements to a fixed array.");
        } else {
            return false;
        }
    }

    @Override
    public boolean removeAll(Collection<?> clctn) {
        for (Object o : clctn) {
            if (this.contains(o)) {
                throw new UnsupportedOperationException("Cannot remove elements to a fixed array.");
            }
        }
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> clctn) {
        throw new UnsupportedOperationException("Not supported yet."); //TODO
    }

    @Override
    public void clear() {
        if (!this.isEmpty()) {
            throw new UnsupportedOperationException("Cannot clear a non-empty fixed array.");
        }
    }

    @Override
    public T get(int i) {
        int real = i + this.from;
        if (this.from <= real && real < this.to) {
            return this.data[real];
        } else {
            throw new IndexOutOfBoundsException("Index must be larger or equal to zero and smaller than the size of the list.");
        }
    }

    @Override
    public T set(int i, T e) {
        int real = i + this.from;
        if (this.from <= real && real < this.to) {
            T old = this.data[real];
            this.data[real] = e;
            return old;
        } else {
            throw new IndexOutOfBoundsException("Index must be larger or equal to zero and smaller than the size of the list.");
        }
    }

    @Override
    public void add(int i, T e) {
        throw new UnsupportedOperationException("Cannot add elements to a fixed array.");
    }

    @Override
    public T remove(int i) {
        throw new UnsupportedOperationException("Cannot remove elements from a fixed array.");
    }

    @Override
    public int indexOf(Object o) {
        for (int i = this.from; i < this.to; i++) {
            if (Objects.equals(data[i], o)) {
                return i;
            }
        }
        return -0x01;
    }

    @Override
    public int lastIndexOf(Object o) {
        for (int i = this.to - 0x01; i >= this.from; i--) {
            if (Objects.equals(data[i], o)) {
                return i;
            }
        }
        return -0x01;
    }

    @Override
    public ListIterator<T> listIterator() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ListIterator<T> listIterator(int i) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<T> subList(int index, int size) {
        int from2 = this.from + index;
        int to2 = from2 + size;
        return new ListArray<>(from2, to2, this.data);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        boolean cond = false;
        if (from < to) {
            sb.append(data[from]);
            for (int i = from + 0x01; i < to; i++) {
                sb.append(",");
                sb.append(data[i]);
            }
        }
        sb.append("]");
        return sb.toString();
    }

}
