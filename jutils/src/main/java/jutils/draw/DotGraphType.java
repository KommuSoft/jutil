package jutils.draw;

/**
 *
 * @author kommusoft
 */
public enum DotGraphType {

    Graph {

                @Override
                public String toString() {
                    return "graph";
                }

            },
    Digraph {

                @Override
                public String toString() {
                    return "digraph";
                }

            },
    StrictGraph {

                @Override
                public boolean isStrict() {
                    return true;
                }

                @Override
                public String toString() {
                    return "strict graph";
                }

            },
    StrictDigraph {

                @Override
                public boolean isStrict() {
                    return true;
                }

                @Override
                public String toString() {
                    return "strict digraph";
                }

            };

    public boolean isStrict() {
        return false;
    }

}
