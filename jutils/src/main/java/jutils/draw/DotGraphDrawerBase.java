package jutils.draw;

public abstract class DotGraphDrawerBase<TInput> extends StructuredDotDrawerBase<TInput> {

    private DotGraphType graphType;

    protected DotGraphDrawerBase() {
        this(DotGraphType.Digraph);
    }

    protected DotGraphDrawerBase(DotGraphType graphType) {
        this.graphType = graphType;
    }

    @Override
    protected String beginning() {
        return String.format("%s {", this.getGraphType().toString());
    }

    /**
     * @return the graphType
     */
    public DotGraphType getGraphType() {
        return graphType;
    }

    /**
     * @param graphType the graphType to set
     */
    public void setGraphType(DotGraphType graphType) {
        this.graphType = graphType;
    }
}
