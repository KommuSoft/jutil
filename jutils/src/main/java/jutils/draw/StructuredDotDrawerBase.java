package jutils.draw;

import java.io.IOException;
import java.io.Writer;

/**
 *
 * @author kommusoft
 * @param <TInput> The type of objects the drawer draws.
 */
public abstract class StructuredDotDrawerBase<TInput> extends DotDrawerBase<TInput> {

    /**
     * The beginning of the structured file.
     *
     * @return The beginning of the structured file.
     * @note By default, the value is an opening accolade.
     */
    protected String beginning() {
        return "{";
    }

    /**
     * The ending of the structure file.
     *
     * @return The ending of the structured file.
     * @note By default, the value is a closing accolade.
     */
    protected String ending() {
        return "}";
    }

    /**
     * Writes a dot file depicting the given input.
     *
     * @param input The object to draw.
     * @param writer The writer to write the 'dot' stream to.
     * @throws java.io.IOException If the stream writer is not accessible or not
     * effective.
     */
    @Override
    public void write(TInput input, Writer writer) throws IOException {
        writer.write(this.beginning());
        writer.append('\n');
        this.innerWrite(input, writer);
        writer.write(this.ending());
        writer.append('\n');
    }

    /**
     * Writes the inner content.
     *
     * @param input The object to draw.
     * @param streamWriter The writer to write the 'dot' stream to.
     * @throws java.io.IOException if the stream writer is not accessible or not
     * effective.
     */
    protected abstract void innerWrite(TInput input, Writer streamWriter) throws IOException;

}
