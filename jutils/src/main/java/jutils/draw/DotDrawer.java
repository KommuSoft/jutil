package jutils.draw;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import jutils.tuples.Tuple2;

/**
 *
 * @author kommusoft
 * @param <TInput> The type of input expected by the drawer.
 */
public interface DotDrawer<TInput> {

    /**
     * Writes a dot file depicting the given TInput.
     *
     * @param input The input to depict.
     * @param filename The resulting 'dot' file filename.
     * @throws java.io.IOException If the filename is invalid, not effective or
     * the user has no proper access to write to the file.
     */
    void write(TInput input, String filename) throws IOException;

    /**
     * Writes a dot file depicting the given TInput.
     *
     * @param input The input to depict.
     * @param stream The stream to write the 'dot' stream to.
     * @throws java.io.IOException If the stream is not accessible or not
     * effective.
     */
    void write(TInput input, OutputStream stream) throws IOException;

    /**
     * Writes a dot file depicting the given TInput.
     *
     * @param input The input to depict.
     * @param streamWriter The writer to write the 'dot' stream to.
     * @throws java.io.IOException If the writer is not accessible or not
     * effective.
     */
    void write(TInput input, Writer streamWriter) throws IOException;

    /**
     * Writes a dot file depicting the given TInput.
     *
     * @param input The input to depict.
     * @return The content of the dot file.
     * @throws java.io.IOException If the writer is not accessible or not
     * effective.
     */
    String write(TInput input) throws IOException;

    void edgeStatement(Writer writer, Object nodeId1, Object nodeId2) throws IOException;

    void edgeStatement(Writer writer, Object nodeId1, Object nodeId2, Iterable<Tuple2<?, ?>> attributes) throws IOException;

    void edgeStatement(Writer writer, Object nodeId1, Object nodeId2, Tuple2<?, ?>... attributes) throws IOException;

    void nodeStatement(Writer writer, Object nodeId) throws IOException;

    void nodeStatement(Writer writer, Object nodeId, Iterable<Tuple2<?, ?>> attributes) throws IOException;

    void nodeStatement(Writer writer, Object nodeId, Tuple2<?, ?>... attributes) throws IOException;

}
