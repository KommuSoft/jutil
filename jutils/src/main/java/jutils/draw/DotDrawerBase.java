package jutils.draw;

import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Iterator;
import jutils.lists.ListArray;
import jutils.tuples.Tuple2;

public abstract class DotDrawerBase<TInput> implements DotDrawer<TInput> {

    public static final String OPEN_ATTRIBUTE_LIST = " [";
    public static final String CLOSE_ATTRIBUTE_LIST = "]";
    public static final String OPEN_SUBGRAPH = "{";
    public static final String CLOSE_SUBGRAPH = "}";
    public static final String BEGIN_OF_STATEMENT = "\t";
    public static final String END_OF_STATEMENT = ";\n";
    public static final String END_OF_ITEM = ", ";
    public static final String ASSIGN_OPERATOR = "=";
    public static final String PORT_OPERATOR = ":";
    public static final String EDGE_OPERATOR = " -> ";
    public static final String KEYWORD_STRICT = "strict";
    public static final String KEYWORD_GRAPH = "graph";
    public static final String KEYWORD_DIGRAPH = "digraph";
    public static final String KEYWORD_SUBGRAPH = "subgraph";
    public static final String KEYWORD_NODE = "node";
    public static final String KEYWORD_EDGE = "edge";

    @Override
    public void write(TInput input, String filename) throws IOException {
        try (FileWriter fw = new FileWriter(filename)) {
            this.write(input, fw);
        }
    }

    @Override
    public void edgeStatement(Writer writer, Object nodeId1, Object nodeId2, Iterable<Tuple2<?, ?>> attributes) throws IOException {
        writer.write(BEGIN_OF_STATEMENT);
        writer.write(nodeId1.toString());
        writer.write(EDGE_OPERATOR);
        writer.write(nodeId2.toString());
        this.writeAList(writer, attributes);
        writer.write(END_OF_STATEMENT);
    }

    @Override
    public void edgeStatement(Writer writer, Object nodeId1, Object nodeId2, Tuple2<?, ?>... attributes) throws IOException {
        this.edgeStatement(writer, nodeId1, nodeId2, new ListArray<>(attributes));
    }

    @Override
    public void edgeStatement(Writer writer, Object nodeId1, Object nodeId2) throws IOException {
        this.edgeStatement(writer, nodeId1, nodeId2, (Iterable<Tuple2<?, ?>>) null);
    }

    @Override
    public void nodeStatement(Writer writer, Object nodeId, Iterable<Tuple2<?, ?>> attributes) throws IOException {
        writer.write(BEGIN_OF_STATEMENT);
        writer.write(nodeId.toString());
        this.writeAList(writer, attributes);
        writer.write(END_OF_STATEMENT);
    }

    @Override
    public void nodeStatement(Writer writer, Object nodeId, Tuple2<?, ?>... attributes) throws IOException {
        this.nodeStatement(writer, nodeId, new ListArray<>(attributes));
    }

    @Override
    public void nodeStatement(Writer writer, Object nodeId) throws IOException {
        this.nodeStatement(writer, nodeId, (Iterable<Tuple2<?, ?>>) null);
    }

    public void writeAttributeList(Writer writer, Iterable<? extends Iterable<Tuple2<?, ?>>> attributes) throws IOException {
        if (attributes != null) {
            for (Iterable<Tuple2<?, ?>> alist : attributes) {
                writeAList(writer, alist);
            }
        }
    }

    public void writeAList(Writer writer, Iterable<Tuple2<?, ?>> alist) throws IOException {
        if (alist != null) {
            writer.append(OPEN_ATTRIBUTE_LIST);
            Iterator<Tuple2<?, ?>> tupleIterator = alist.iterator();
            Tuple2<?, ?> tuple;
            boolean next = tupleIterator.hasNext();
            while (next) {
                tuple = tupleIterator.next();
                writer.write(tuple.getItem1().toString());
                writer.write(ASSIGN_OPERATOR);
                writer.write(tuple.getItem2().toString());
                next = tupleIterator.hasNext();
                if (next) {
                    writer.write(END_OF_ITEM);
                }
            }
            writer.append(CLOSE_ATTRIBUTE_LIST);
        }
    }

    public void writeAList(Writer writer, Tuple2<?, ?>... alist) throws IOException {
        if (alist != null) {
            writer.append(OPEN_ATTRIBUTE_LIST);
            for (Tuple2<?, ?> tuple : alist) {
                writer.write(tuple.getItem1().toString());
                writer.write(ASSIGN_OPERATOR);
                writer.write(tuple.getItem2().toString());
            }
            writer.append(CLOSE_ATTRIBUTE_LIST);
        }
    }

    @Override
    public void write(TInput input, OutputStream stream) throws IOException {
        try (OutputStreamWriter osw = new OutputStreamWriter(stream)) {
            this.write(input, osw);
        }
    }

    @Override
    public String write(TInput input) throws IOException {
        try (StringWriter sw = new StringWriter()) {
            this.write(input, sw);
            sw.flush();
            return sw.getBuffer().toString();
        }
    }

}
