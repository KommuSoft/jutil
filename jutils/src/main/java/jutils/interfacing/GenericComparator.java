package jutils.interfacing;

import java.util.Comparator;

/**
 *
 * @author kommusoft
 * @param <T>
 */
public class GenericComparator<T extends Comparable<T>> implements Comparator<T> {

    public GenericComparator() {
    }

    @Override
    public int compare(T t1, T t2) {
        if (t1 != null) {
            return t1.compareTo(t2);
        } else if (t2 != null) {
            return -t2.compareTo(t1);
        } else {
            return 0x00;
        }
    }

}
