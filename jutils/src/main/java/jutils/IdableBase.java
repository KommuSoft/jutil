package jutils;

import java.util.logging.Logger;

public class IdableBase implements Idable {

    private static long idDispatcher = 0x00;
    private static final Logger LOG = Logger.getLogger(IdableBase.class.getName());

    private static synchronized long dispatchId() {
        try {
            return idDispatcher;
        } finally {
            idDispatcher++;
        }
    }
    private final long id;

    protected IdableBase() {
        this.id = dispatchId();
    }

    @Override
    public long getId() {
        return this.id;
    }

}
