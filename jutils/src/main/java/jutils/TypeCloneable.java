package jutils;

/**
 *
 * @author kommusoft
 */
public interface TypeCloneable<TResult> extends Cloneable {

    public abstract TResult typeClone() throws CloneNotSupportedException;

}
