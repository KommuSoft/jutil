package jutils.types;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import jutils.collections.valuesets.BooleanSet;
import jutils.collections.valuesets.ByteSet;
import jutils.collections.valuesets.CharacterSet;

/**
 *
 * @author kommusoft
 */
public class TypeUtils {

    private static Map<Class<?>, Class<?>> wrappers = null;
    private static final Logger LOG = Logger.getLogger(TypeUtils.class.getName());

    public static Map<Class<?>, Class<?>> getWrapper() {
        if (wrappers == null) {
            HashMap<Class<?>, Class<?>> original = new HashMap<>(0x09);
            original.put(boolean.class, Boolean.class);
            original.put(byte.class, Byte.class);
            original.put(char.class, Character.class);
            original.put(double.class, Double.class);
            original.put(float.class, Float.class);
            original.put(int.class, Integer.class);
            original.put(long.class, Long.class);
            original.put(short.class, Short.class);
            original.put(void.class, Void.class);
            wrappers = Collections.unmodifiableMap(original);
        }
        return wrappers;
    }

    public static Class<?> getWrapper(Class<?> classdef) {
        if (classdef != null && classdef.isPrimitive()) {
            return getWrapper().get(classdef);
        } else {
            return classdef;
        }
    }

    public static <T> boolean isNominal(Class<T> classdef) {
        Class<?> classdef2 = getWrapper(classdef);
        return (classdef2 != null && (classdef2.isEnum() || classdef2 == Character.class || classdef2 == Boolean.class || classdef2 == Byte.class));
    }

    public static <T> boolean isBuiltinContinu(Class<T> classdef) {
        Class<?> classdef2 = getWrapper(classdef);
        return classdef2 == Double.class || classdef2 == Float.class;
    }

    public static <T> boolean isOrdinal(Class<T> classdef) {
        Class<?> classdef2 = getWrapper(classdef);
        return (classdef2 != null && Comparable.class.isAssignableFrom(classdef2));
    }

    @SuppressWarnings("unchecked")
    public static <T> Set<T> getNominalSet(Class< T> classdef) {
        if (classdef == null) {
            return null;
        }
        Class<?> classdef2 = getWrapper(classdef);
        if (classdef2 == Boolean.class) {
            return (Set<T>) BooleanSet.Instance;
        } else if (classdef2 == Character.class) {
            return (Set<T>) CharacterSet.Instance;
        } else if (classdef2 == Byte.class) {
            return (Set<T>) ByteSet.Instance;
        } else if (classdef2.isEnum()) {
            HashSet<T> res = new HashSet<>(Arrays.asList(classdef.getEnumConstants()));
            return res;
        } else {
            return null;
        }
    }

    private TypeUtils() {
    }
}
