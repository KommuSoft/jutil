package jutils.collections;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import jutils.FactoryMethod;
import jutils.iterators.AppendIterable;
import jutils.iterators.MapIterable;
import jutils.algebra.Function;

/**
 * A set of utility functions regarding collections, iterables and maps.
 *
 * @author kommusoft
 */
public class CollectionUtils {

    private static final Logger LOG = Logger.getLogger(CollectionUtils.class.getName());

    public static final String ITERABLE_BEGIN = "[ ";
    public static final String ITERABLE_NEXT = " , ";
    public static final String ITERABLE_END = " ]";

    /**
     * A private utility method that in case of an iterator appends a comma
     * separated list of its items.
     *
     * @param appendable The appendable to write the textual representation to.
     * @param iterator The iterator that provides the items to print.
     * @throws IOException In case the appendable fails to write text to.
     * @note The method calls the
     * {@link #deepToString(Appendable, Object) deepToString} method to print
     * the items of the iterator.
     */
    private static void writeIterator(Appendable appendable, Iterator<?> iterator) throws IOException {
        if (iterator != null && iterator.hasNext()) {
            deepToString(appendable, iterator.next());
            while (iterator.hasNext()) {
                appendable.append(ITERABLE_NEXT);
                deepToString(appendable, iterator.next());
            }
        }
    }

    /**
     * Represents the given item as a String where collections are detected and
     * written out.
     *
     * @param item The given item to generate a string from.
     * @return A string that is the textual representation of the given item.
     */
    public static String deepToString(Object item) {
        StringBuilder sb = new StringBuilder();
        try {
            deepToString(sb, item);
        } catch (IOException ex) {
            Logger.getLogger(CollectionUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sb.toString();
    }

    /**
     * Writes a textual representation of the given item to the appendable. In
     * case a collection is detected, the collection is written out.
     *
     * @param appendable The given appendable to write the textual
     * representation to.
     * @param item The given item to generate the textual representation from.
     * @throws java.io.IOException In case the appendable throws an IOException.
     */
    public static void deepToString(Appendable appendable, Object item) throws IOException {
        if (item != null && item instanceof Iterable<?>) {
            appendable.append(ITERABLE_BEGIN);
            Iterator<?> iterator = ((Iterable<?>) item).iterator();
            writeIterator(appendable, iterator);
            appendable.append(ITERABLE_END);
        } else if (item != null && item instanceof Iterator<?>) {
            appendable.append(ITERABLE_BEGIN);
            Iterator<?> iterator = (Iterator<?>) item;
            writeIterator(appendable, iterator);
            appendable.append(ITERABLE_END);
        } else {
            appendable.append(Objects.toString(item));
        }
    }

    /**
     * Copies the entries of the given source into the given target.
     *
     * @param <TKey> The type of the keys of the given target map.
     * @param <TValue> Th type of the values of the given target map.
     * @param target The given target to copy the entries to.
     * @param source The given map to copy the entries from.
     * @note In case the key already exists in the given target, the value is
     * overwritten.
     */
    public static <TKey, TValue> void putAll(final Map<TKey, TValue> target, final Map<? extends TKey, ? extends TValue> source) {
        if (target != null && source != null) {
            putAll(target, source.entrySet());
        }
    }

    /**
     * Copies the entries of the given source into a new HashMap.
     *
     * @param <TKey> The type of the keys of the given target map.
     * @param <TValue> The type of the values of the given.
     * @param source The given iterable of entries, to copy the entries from.
     * @return A hashmap that contains all the entries of the given iterable.
     * @note In multiple entries occur with the same value. The value of the
     * last tuple is picked.
     */
    public static <TKey, TValue> HashMap<TKey, TValue> putAll(final Iterable<? extends Entry<? extends TKey, ? extends TValue>> source) {
        HashMap<TKey, TValue> map = new HashMap<>();
        putAll(map, source);
        return map;
    }

    /**
     * Copies the entries of the given source into the given target.
     *
     * @param <TKey> The type of the keys of the given target map.
     * @param <TValue> The type of the values of the given
     * @param target The given to target to copy the entries to.
     * @param source The given iterable of entries, to copy the entries from.
     * @note In case the key already exists in the given target, the value is
     * overwritten.
     */
    public static <TKey, TValue> void putAll(final Map<TKey, TValue> target, final Iterable<? extends Entry<? extends TKey, ? extends TValue>> source) {
        if (target != null && source != null) {
            for (Entry<? extends TKey, ? extends TValue> entry : source) {
                target.put(entry.getKey(), entry.getValue());
            }
        }
    }

    /**
     * Removes all the given keys from the given map.
     *
     * @param <TKey> The type of the keys of the map.
     * @param <TValue> The types of the values of the map.
     * @param target The map to remove the specified keys from.
     * @param keys An iterable providing the keys to be removed.
     * @note In case the target or the keys are not effective, nothing happens.
     */
    public static <TKey, TValue> void removeAll(final Map<TKey, TValue> target, final Iterable<? extends TKey> keys) {
        if (keys != null) {
            removeAll(target, keys.iterator());
        }
    }

    /**
     * Removes all the given keys from the given map.
     *
     * @param <TKey> The type of the keys of the map.
     * @param <TValue> The types of the values of the map.
     * @param target The map to remove the specified keys from.
     * @param keys An iterator providing the keys to be removed.
     * @note In case the target or the keys are not effective, nothing happens.
     */
    public static <TKey, TValue> void removeAll(final Map<TKey, TValue> target, final Iterator<? extends TKey> keys) {
        if (target != null && keys != null) {
            while (keys.hasNext()) {
                target.remove(keys.next());
            }
        }
    }

    /**
     * Removes all the given keys from the given map and adds their
     * corresponding values to the given collection.
     *
     * @param <TKey> The type of the keys of the map.
     * @param <TValue> The types of the values of the map.
     * @param target The map to remove the specified keys from.
     * @param keys An iterable providing the keys to be removed.
     * @param values A collection where the values corresponding to the given
     * keys are added.
     * @note In case the target or the keys are not effective, nothing happens.
     * @note In case values is not effective, the keys are removed without
     * adding them.
     */
    public static <TKey, TValue> void removeAll(final Map<TKey, TValue> target, final Iterable<? extends TKey> keys, Collection<? super TValue> values) {
        if (keys != null) {
            removeAll(target, keys.iterator(), values);
        }
    }

    /**
     * Removes all the given keys from the given map and adds their
     * corresponding values to the given collection.
     *
     * @param <TKey> The type of the keys of the map.
     * @param <TValue> The types of the values of the map.
     * @param target The map to remove the specified keys from.
     * @param keys An iterator providing the keys to be removed.
     * @param values A collection where the values corresponding to the given
     * keys are added.
     * @note In case the target or the keys are not effective, nothing happens.
     * @note In case values is not effective, the keys are removed without
     * adding them.
     */
    public static <TKey, TValue> void removeAll(final Map<TKey, TValue> target, final Iterator<? extends TKey> keys, Collection<? super TValue> values) {
        if (values != null) {
            if (target != null && keys != null) {
                while (keys.hasNext()) {
                    values.add(target.remove(keys.next()));
                }
            }
        } else {
            removeAll(target, keys);
        }
    }

    /**
     * Sets all the values of the given map to the given value.
     *
     * @param <T1> The type of the keys of the given map.
     * @param <T2> The type of the values of the given map.
     * @param map The given map to modify.
     * @param value The value where all keys should point to.
     * @return The given map after modification (for chaining purposes).
     */
    public static <T1, T2> Map<T1, T2> setAllValues(Map<T1, T2> map, T2 value) {
        if (map != null) {
            for (Entry<T1, T2> entry : map.entrySet()) {
                entry.setValue(value);
            }
        }
        return map;
    }

    /**
     * Calculates the number of generated elements of the given iterable.
     *
     * @param <T> The type of elements enumerated by the given iterable.
     * @param iterable The given iterable.
     * @return The number of elements generated by the iterable.
     */
    public static <T> int size(final Iterable<T> iterable) {
        if (iterable != null) {
            return size(iterable.iterator());
        } else {
            return 0x00;
        }
    }

    /**
     * Calculates the number of generated elements of the given iterator.
     *
     * @param <T> The type of elements enumerated by the given iterator.
     * @param iterator The given iterator.
     * @return The number of elements generated by the iterator.
     */
    public static <T> int size(final Iterator<T> iterator) {
        int n = 0x00;
        if (iterator != null) {
            while (iterator.hasNext()) {
                iterator.next();
                n++;
            }
        }
        return n;
    }

    /**
     * Maps a given iterable of elements to another iterable using the given
     * function.
     *
     * @param <T1> The type of the elements of the first iterable.
     * @param <T2> The type of the elements of the resulting iterable.
     * @param iterable The iterable that contains the initial elements.
     * @param function The function that specifies how the source items should
     * be mapped on the target items.
     * @return An iterable of the function items.
     * @note This function is executed lazily: if elements of the source are
     * modified, before they are iterated, their corresponding elements will be
     * the function application of the modified elements.
     */
    public static <T1, T2> Iterable<T2> map(Iterable<T1> iterable, Function<T1, T2> function) {
        return new MapIterable<>(iterable, function);
    }

    /**
     * Appends an iterable of iterables to an iterable.
     *
     * @param <T> The type of the elements.
     * @param iterable An iterable of iterables to append.
     * @return An iterable where all the elements of the iterables are appended.
     * @note This function is executed lazily: if elements of the source are
     * modified, before they are iterated, their corresponding resulting
     * elements will be the modified elements.
     */
    public static <T> Iterable<T> append(Iterable<? extends Iterable<T>> iterable) {
        return new AppendIterable<>(iterable);
    }

    /**
     * Returns the maximum element of some iterable with respect to a function
     * that maps the elements to a comparable value.
     *
     * @param <T1> The original type of elements.
     * @param <T2> A type that can be compared.
     * @param iterable An iterable that yields the elements to calculate the
     * maximum from.
     * @param function A function to apply in order to compare two elements.
     * @return The maximum element of the given iterable with respect to the
     * corresponding values after function application.
     */
    public static <T1, T2 extends Comparable<T2>> T1 argMax(Iterable<T1> iterable, Function<T1, T2> function) {
        if (iterable != null) {
            return argMax(iterable.iterator(), function);
        } else {
            return null;
        }
    }

    /**
     * Returns the maximum element of some iterator with respect to a function
     * that maps the elements to a comparable value.
     *
     * @param <T1> The original type of elements.
     * @param <T2> A type that can be compared.
     * @param iterator An iterator that yields the elements to calculate the
     * maximum from.
     * @param function A function to apply in order to compare two elements.
     * @return The maximum element of the given iterator with respect to the
     * corresponding values after function application.
     */
    public static <T1, T2 extends Comparable<T2>> T1 argMax(Iterator<T1> iterator, Function<T1, T2> function) {
        T1 maxArg = null;
        if (function != null && iterator != null && iterator.hasNext()) {
            maxArg = iterator.next();
            T1 arg;
            T2 maxVal = function.evaluate(maxArg), val;
            while (iterator.hasNext()) {
                arg = iterator.next();
                if (arg != null) {
                    val = function.evaluate(arg);
                    if (maxVal == null || maxVal.compareTo(val) < 0x00) {
                        maxVal = val;
                        maxArg = arg;
                    }
                }
            }
        }
        return maxArg;
    }

    /**
     * Returns the minimum element of some iterable with respect to a function
     * that maps the elements to a comparable value.
     *
     * @param <T1> The original type of elements.
     * @param <T2> A type that can be compared.
     * @param iterable An iterable that yields the elements to calculate the
     * minimum from.
     * @param function A function to apply in order to compare two elements.
     * @return The minimum element of the given iterable with respect to the
     * corresponding values after function application.
     */
    public static <T1, T2 extends Comparable<T2>> T1 argMin(Iterable<T1> iterable, Function<T1, T2> function) {
        if (iterable != null) {
            return argMin(iterable.iterator(), function);
        } else {
            return null;
        }
    }

    /**
     * Returns the minimum element of some iterator with respect to a function
     * that maps the elements to a comparable value.
     *
     * @param <T1> The original type of elements.
     * @param <T2> A type that can be compared.
     * @param iterator An iterator that yields the elements to calculate the
     * minimum from.
     * @param function A function to apply in order to compare two elements.
     * @return The minimum element of the given iterator with respect to the
     * corresponding values after function application.
     */
    public static <T1, T2 extends Comparable<T2>> T1 argMin(Iterator<T1> iterator, Function<T1, T2> function) {
        T1 minArg = null;
        if (function != null && iterator != null && iterator.hasNext()) {
            minArg = iterator.next();
            T1 arg;
            T2 minVal = function.evaluate(minArg), val;
            while (iterator.hasNext()) {
                arg = iterator.next();
                if (arg != null) {
                    val = function.evaluate(arg);
                    if (minVal == null || minVal.compareTo(val) > 0x00) {
                        minVal = val;
                        minArg = arg;
                    }
                }
            }
        }
        return minArg;
    }

    /**
     * Adds the given value to the collection of the given key, if such
     * collection already exists, otherwise a new collection is added with the
     * given value.
     *
     * @param <TKey> The type of the keys of the given map.
     * @param <TCollection> The type of the collections of the given map.
     * @param <TValue> The type of the elements to be added.
     * @param map The given map to modify.
     * @param key The given key.
     * @param value The value to add to the collection of the corresponding key.
     * @param collectionFactory A factory that generates a new collection in
     * case the key does not exists in the dictionary.
     * @return The given map after modifications (for chaining purposes).
     */
    public static <TKey, TCollection extends Collection<TValue>, TValue> Map<TKey, TCollection> addtoList(Map<TKey, TCollection> map, TKey key, TValue value, FactoryMethod<? extends TCollection> collectionFactory) {
        TCollection col = map.get(key);
        if (col == null) {
            col = collectionFactory.generate();
            map.put(key, col);
        }
        col.add(value);
        return map;
    }

    /**
     * Classifies the given stream of values such that for each value of the
     * given function, a collection is associated with the corresponding stream
     * values.
     *
     * @param <TKey> The type of the keys of the map (and the range of the
     * classifier).
     * @param <TCollection> The type of the collections of the map.
     * @param <TValue> The type of elements to classify.
     * @param items The given stream of elements to classify.
     * @param classifier The given function that classifies the given stream of
     * elements.
     * @param collectionFactory A factory method that generates a new collection
     * in case there is no collection associated with the given class yet.
     * @return A map that maps each key to a collection containing values
     * associated with this key with respect to the classifier.
     */
    public static <TKey, TCollection extends Collection<TValue>, TValue> Map<TKey, TCollection> classify(Iterable<? extends TValue> items, Function<? super TValue, ? extends TKey> classifier, FactoryMethod<? extends TCollection> collectionFactory) {
        HashMap<TKey, TCollection> map = new HashMap<>();
        for (TValue item : items) {
            addtoList(map, classifier.evaluate(item), item, collectionFactory);
        }
        return map;
    }

    /**
     * Classifies the given stream of values such that for each value of the
     * given function, a collection is associated with the corresponding stream
     * values.
     *
     * @param <TKey> The type of the keys of the map (and the range of the
     * classifier).
     * @param <TCollection> The type of the collections of the map.
     * @param <TValue> The type of elements to classify.
     * @param map The given map to modify.
     * @param items The given stream of elements to classify.
     * @param classifier The given function that classifies the given stream of
     * elements.
     * @param collectionFactory A factory method that generates a new collection
     * in case there is no collection associated with the given class yet.
     * @return The given map after modification (for chaining purposes).
     */
    public static <TKey, TCollection extends Collection<TValue>, TValue> Map<TKey, TCollection> classify(Map<TKey, TCollection> map, Iterable<? extends TValue> items, Function<? super TValue, ? extends TKey> classifier, FactoryMethod<? extends TCollection> collectionFactory) {
        for (TValue item : items) {
            addtoList(map, classifier.evaluate(item), item, collectionFactory);
        }
        return map;
    }

    /**
     * Increments the integer value of the given key. In case the key does not
     * exists, one is associated with the given key.
     *
     * @param <T> The type of the keys of the given map.
     * @param map The given map to increment the given key of.
     * @param key The given key.
     * @return The new value associated with the given key.
     */
    public static <T> int incrementKey(Map<T, Integer> map, T key) {
        return incrementKey(map, key, 0x01);
    }

    /**
     * Increments the integer value of the given key by the given increment. In
     * case the given key does not exists, the key is associated with the given
     * key.
     *
     * @param <T> The type of the keys of the given map.
     * @param map The given map to increment the given key of.
     * @param key The given key.
     * @param increment The value to increment the value associated to the given
     * key with.
     * @return The new value associated with the given key.
     */
    public static <T> int incrementKey(Map<T, Integer> map, T key, int increment) {
        return incrementKey(map, key, increment, increment);
    }

    /**
     * Increments the integer value of the given key by the given increment. In
     * case the given key does not exists, the key is associated with the given
     * default value.
     *
     * @param <T> The type of the keys of the given map.
     * @param map The given map to increment the given key of.
     * @param key The given key.
     * @param increment The value to increment the value associated to the given
     * key with.
     * @param deflt The value to associate the given key with, in case the key
     * is not yet added to the map.
     * @return The new value associated with the given key.
     */
    public static <T> int incrementKey(Map<T, Integer> map, T key, int increment, int deflt) {
        Integer val = map.get(key);
        if (val == null) {
            val = deflt;
        } else {
            val += increment;
        }
        map.put(key, val);
        return val;
    }

    /**
     * Associates the given key with the function evaluation of the original
     * key. In case the given key does not exists, the key is associated wit the
     * given default value.
     *
     * @param <TKey> The type of the keys of the given map.
     * @param <TValue> The type of the values of the given map.
     * @param map The given map to increment the given key of.
     * @param key The given key.
     * @param increment A function that represents how a value should be
     * implemented.
     * @param deflt The value to associate with the given key, in case the key
     * is not yet added to the map.
     * @return The new value associated with the given key.
     * @note Be careful with the increment function: in case the original value
     * is modified, the object is modified. This may result in side effects.
     */
    public static <TKey, TValue> TValue incrementKey(Map<TKey, TValue> map, TKey key, Function<? super TValue, ? extends TValue> increment, TValue deflt) {
        TValue val = map.get(key);
        if (val == null) {
            val = deflt;
        } else {
            val = increment.evaluate(val);
        }
        map.put(key, val);
        return val;
    }

    /**
     * Decrements the integer value of the given key. In case the key does not
     * exists, minus one is associated with the given key.
     *
     * @param <T> The type of the keys of the given map.
     * @param map The given map to decrement the given key of.
     * @param key The given key.
     * @return The new value associated with the given key.
     */
    public static <T> int decrementKey(Map<T, Integer> map, T key) {
        return decrementKey(map, key, 0x01);
    }

    /**
     * Decrements the integer value of the given key by the given decrement. In
     * case the given key does not exists, the key is associated with minus the
     * given key.
     *
     * @param <T> The type of the keys of the given map.
     * @param map The given map to decrement the given key of.
     * @param key The given key.
     * @param decrement The value to decrement the value associated to the given
     * key with.
     * @return The new value associated with the given key.
     */
    public static <T> int decrementKey(Map<T, Integer> map, T key, int decrement) {
        return decrementKey(map, key, decrement, -decrement);
    }

    /**
     * Decrements the integer value of the given key by the given increment. In
     * case the given key does not exists, the key is associated with the given
     * default value.
     *
     * @param <T> The type of the keys of the given map.
     * @param map The given map to decrement the given key of.
     * @param key The given key.
     * @param decrement The value to decrement the value associated to the given
     * key with.
     * @param deflt The value to associate the given key with, in case the key
     * is not yet added to the map.
     * @return The new value associated with the given key.
     */
    public static <T> int decrementKey(Map<T, Integer> map, T key, int decrement, int deflt) {
        Integer val = map.get(key);
        if (val == null) {
            val = deflt;
        } else {
            val -= decrement;
        }
        map.put(key, val);
        return val;
    }

    public static <T extends Comparable<T>> int getLowestInsertionPoint(final List<T> list, final T key) {
        int index = Collections.binarySearch(list, key);
        if (index >= 0x00) {
            return index;
        } else {
            return ~index;
        }
    }

    public static int getLowestInsertionPoint(final double[] list, final double key) {
        int index = Arrays.binarySearch(list, key);
        if (index >= 0x00) {
            return index;
        } else {
            return ~index;
        }
    }

    public static <TKey, TValue> void replaceKey(final Map<TKey, TValue> map, final TKey oldkey, final TKey newkey) throws IllegalArgumentException {
        if (map != null && map.containsKey(oldkey)) {
            map.put(newkey, map.get(oldkey));

        } else {
            throw new IllegalArgumentException("Map must be effective and contain the old key.");
        }
    }

    public static <T extends Comparable<T>> void incrementKeyByIndex(final Map<T, Integer> map, final List<T> keys) {
        incrementKeyByIndex(map, keys, 0x01);
    }

    public static <T extends Comparable<T>> void incrementKeyByIndex(final Map<T, Integer> map, final List<T> keys, final int delta) {
        for (Entry<T, Integer> entry : map.entrySet()) {
            entry.setValue(entry.getValue() + delta * getLowestInsertionPoint(keys, entry.getKey()));
        }
    }

    public static <T> void incrementValueByIndex(final Map<T, Integer> map, final List<Integer> keys) {
        incrementValueByIndex(map, keys, 0x01);
    }

    public static <T> void incrementValueByIndex(final Map<T, Integer> map, final List<Integer> keys, final int delta) {
        for (Entry<T, Integer> entry : map.entrySet()) {
            int val = entry.getValue();
            entry.setValue(val + delta * getLowestInsertionPoint(keys, val));
        }
    }

    public static <TKey extends TKeyT, TKeyT> Map<? super TKey, Integer> mergeMapWithAdd(Map<TKeyT, Integer> target, Map<? extends TKey, ? extends Integer> source) {
        for (Entry<? extends TKey, ? extends Integer> entry2 : source.entrySet()) {
            TKey key2 = entry2.getKey();
            int val2 = entry2.getValue();
            if (target.containsKey(key2)) {
                int val1 = target.get(key2);
                target.put(key2, val1 + val2);
            } else {
                target.put(key2, val2);
            }
        }
        return target;
    }

    public static <TKey extends TKeyT, TKeyT, TValue1 extends TValueT, TValueT> Map<? super TKey, TValueT> mergeMapWith(Map<TKeyT, TValueT> target, Map<? extends TKey, ? extends TValue1> source, Function<? super TValueT, Function<TValue1, ? extends TValueT>> merger) {
        for (Entry<? extends TKey, ? extends TValue1> entry2 : source.entrySet()) {
            TKey key2 = entry2.getKey();
            TValue1 val2 = entry2.getValue();
            if (target.containsKey(key2)) {
                TValueT val1 = target.get(key2);
                target.put(key2, merger.evaluate(val1).evaluate(val2));
            } else {
                target.put(key2, val2);
            }
        }
        return target;
    }

    public static <TKey extends TKeyT, TKeyT, TValue1 extends TValue, TValue2 extends TValue, TValue extends TValueT, TValueT> Map<? super TKey, TValueT> mergeMaps(Map<? extends TKey, ? extends TValue1> source1, Map<? extends TKey, ? extends TValue2> source2, Map<TKeyT, TValueT> target, Function<TValue1, Function<TValue2, TValue>> merger) {
        for (Entry<? extends TKey, ? extends TValue1> entry1 : source1.entrySet()) {
            TKey key1 = entry1.getKey();
            TValue1 val1 = entry1.getValue();
            if (source2.containsKey(key1)) {
                target.put(key1, merger.evaluate(val1).evaluate(source2.get(key1)));
            } else {
                target.put(key1, val1);
            }
        }
        for (Entry<? extends TKey, ? extends TValue2> entry2 : source2.entrySet()) {
            TKey key2 = entry2.getKey();
            TValue2 val2 = entry2.getValue();
            if (!source1.containsKey(key2)) {
                target.put(key2, val2);
            }
        }
        return target;
    }

    /**
     * Adds all the elements of the given source to the given target collection.
     *
     * @param <TElement> The type of the elements of the given collection.
     * @param <TCollection> The type of the given collection.
     * @param target The given collection to copy values to.
     * @param source The given source of values.
     * @return The given target collection (for chaining purposes).
     * @note If either the source or the target are not effective, no
     * modifications are made.
     */
    public static <TElement, TCollection extends Collection<TElement>> TCollection addAll(final TCollection target, final Iterable<? extends TElement> source) {
        if (target != null && source != null) {
            for (TElement element : source) {
                target.add(element);
            }
        }
        return target;
    }

    /**
     * Adds all the elements of the given iterables of the given source to the
     * given target collection.
     *
     * @param <TElement> The type of the elements of the given collection.
     * @param <TCollection> The type of the given collection.
     * @param target The given collection to copy values to.
     * @param sources The given source of iterables of values.
     * @return The given target collection (for chaining purposes).
     * @note If either the source or the target are not effective, no
     * modifications are made.
     */
    public static <TElement, TCollection extends Collection<TElement>> TCollection addAllFlat(final TCollection target, final Iterable<? extends Iterable<? extends TElement>> sources) {
        if (target != null && sources != null) {
            for (Iterable<? extends TElement> source : sources) {
                for (TElement element : source) {
                    target.add(element);
                }
            }
        }
        return target;
    }

    private CollectionUtils() {
    }

}
