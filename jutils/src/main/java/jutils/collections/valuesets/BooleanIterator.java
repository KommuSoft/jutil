package jutils.collections.valuesets;

import java.util.Iterator;
import java.util.logging.Logger;

/**
 *
 * @author kommusoft
 */
public class BooleanIterator implements Iterator<Boolean> {

    private static final Logger LOG = Logger.getLogger(BooleanIterator.class.getName());

    private byte value = 0x00;

    public BooleanIterator() {
    }

    @Override
    public boolean hasNext() {
        return value < 0x02;
    }

    @Override
    public Boolean next() {
        boolean result = (this.value == 0x01);
        this.value++;
        return result;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Cannot remove values from the BooleanIterator.");
    }

}
