package jutils.collections.valuesets;

import java.util.Collection;
import java.util.Set;

/**
 *
 * @author kommusoft
 * @param <T>
 */
public abstract class ValueSetBase<T> implements Set<T> {

    @Override
    public boolean add(T e) {
        throw new UnsupportedOperationException("Immutable.");
    }

    @Override
    public boolean addAll(Collection<? extends T> clctn) {
        throw new UnsupportedOperationException("Immutable.");
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException("Immutable.");
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean remove(Object o) {
        throw new UnsupportedOperationException("Immutable.");
    }

    @Override
    public boolean removeAll(Collection<?> clctn) {
        throw new UnsupportedOperationException("Immutable.");
    }

    @Override
    public boolean retainAll(Collection<?> clctn) {
        throw new UnsupportedOperationException("Immutable.");
    }

    @Override
    public boolean containsAll(Collection<?> clctn) {
        for (Object val : clctn) {
            if (!this.contains(val)) {
                return false;
            }
        }
        return true;
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T[] toArray(T[] ts) {
        return (T[]) this.toArray();
    }

}
