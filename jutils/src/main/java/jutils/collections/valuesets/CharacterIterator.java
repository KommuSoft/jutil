package jutils.collections.valuesets;

import java.util.Iterator;

/**
 *
 * @author kommusoft
 */
public class CharacterIterator implements Iterator<Character> {

    int c = Character.MIN_VALUE;

    public CharacterIterator() {
    }

    @Override
    public boolean hasNext() {
        return (c <= Character.MAX_VALUE);
    }

    @Override
    public Character next() {
        char result = (char) c;
        c++;
        return result;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Cannot remove from CharacterIterator.");
    }

}
