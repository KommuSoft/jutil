package jutils.collections.valuesets;

import java.util.Iterator;
import java.util.logging.Logger;

/**
 *
 * @author kommusoft
 */
public class ByteSet extends ValueSetBase<Byte> {

    public final static ByteSet Instance = new ByteSet();
    private static final Logger LOG = Logger.getLogger(ByteSet.class.getName());

    private ByteSet() {
    }

    @Override
    public int size() {
        return 0x100;
    }

    @Override
    public boolean contains(Object o) {
        return (o != null && o instanceof Byte);
    }

    @Override
    public Iterator<Byte> iterator() {
        return new ByteIterator();
    }

    @Override
    public Object[] toArray() {
        Object[] result = new Object[this.size()];
        for (int i = 0x00; i < this.size(); i++) {
            result[i] = (Byte) (byte) i;
        }
        return result;
    }

}
