package jutils.collections.valuesets;

import java.util.Iterator;
import java.util.logging.Logger;

/**
 *
 * @author kommusoft
 */
public class BooleanSet extends ValueSetBase<Boolean> {

    public static final BooleanSet Instance = new BooleanSet();
    private static final Logger LOG = Logger.getLogger(BooleanSet.class.getName());

    private BooleanSet() {
    }

    @Override
    public int size() {
        return 0x02;
    }

    @Override
    public boolean contains(Object o) {
        return o.equals(Boolean.FALSE) || o.equals(Boolean.TRUE);
    }

    @Override
    public Iterator<Boolean> iterator() {
        return new BooleanIterator();
    }

    @Override
    public Object[] toArray() {
        return new Object[]{Boolean.FALSE, Boolean.TRUE};
    }

}
