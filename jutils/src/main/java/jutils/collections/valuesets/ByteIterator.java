package jutils.collections.valuesets;

import java.util.Iterator;

/**
 *
 * @author kommusoft
 */
public class ByteIterator implements Iterator<Byte> {

    private short value = 0x00;

    public ByteIterator() {
    }

    @Override
    public boolean hasNext() {
        return value < 0x100;
    }

    @Override
    public Byte next() {
        byte result = (byte) value;
        this.value++;
        return result;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Cannot remove values from a ByteIterator.");
    }

}
