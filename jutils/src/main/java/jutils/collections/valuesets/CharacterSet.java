package jutils.collections.valuesets;

import java.util.Iterator;

/**
 *
 * @author kommusoft
 */
public class CharacterSet extends ValueSetBase<Character> {

    public static CharacterSet Instance = new CharacterSet();

    private CharacterSet() {
    }

    @Override
    public int size() {
        return (int) Character.MAX_VALUE - Character.MIN_VALUE + 0x01;
    }

    @Override
    public boolean contains(Object o) {
        if (o != null && o instanceof Character) {
            Character c = (Character) o;
            int val = c.charValue();
            return Character.MIN_VALUE <= val && val <= Character.MAX_VALUE;
        } else {
            return false;
        }
    }

    @Override
    public Iterator<Character> iterator() {
        return new CharacterIterator();
    }

    @Override
    public Object[] toArray() {
        Object[] values = new Object[this.size()];
        for (int i = Character.MIN_VALUE; i <= Character.MAX_VALUE; i++) {
            values[i] = (char) i;
        }
        return values;
    }

}
