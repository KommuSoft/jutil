package jutils;

/**
 *
 * @author kommusoft
 */
public interface Validateable {

    boolean isValid();

}
