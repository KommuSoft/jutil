package jutils;

import java.util.Iterator;
import java.util.logging.Logger;
import jutils.lists.ListArray;

/**
 * A class that provides utility methods like copying that can be performed on
 * arrays.
 *
 * @author kommusoft
 */
public class ArrayUtils {

    private static final Logger LOG = Logger.getLogger(ArrayUtils.class.getName());

    /**
     * Copy the content of the source array to the target array, but skip the
     * given indices.
     *
     * @param <T> The type of elements of both arrays.
     * @param target The target array where the elements are written to.
     * @param source The source array where the elements are read from.
     * @param indices The iterator of indices to skip.
     * @note If the source or target is not effective, nothing happens
     * @note If the list of indices is not effective, the array is copied
     * entirely.
     * @note If the target array is not large enough, elements are copied until
     * the end is reached.
     * @note Indices in the list of indices that are out of range are ignored.
     * @pre If the list of indices is effective, it must be ordered.
     */
    public static <T> void copySkipArray(T[] target, T[] source, int... indices) {
        if (target != null && source != null) {
            int ns = source.length;
            int nt = target.length;
            if (indices != null) {
                int i = 0x00, j = 0x00, l;
                for (int k : indices) {
                    if (k >= 0x00 && k < ns) {
                        l = k - j;
                        System.arraycopy(source, j, target, i, l);
                        j = k + 0x01;
                        i += l;
                    }
                }
                System.arraycopy(source, j, target, i, Math.min(ns - j, nt - i));
            } else {
                System.arraycopy(source, 0, target, 0, Math.min(ns, nt));
            }
        }
    }

    /**
     * Copy the content of the source array to the target array, but skip the
     * given indices.
     *
     * @param <T> The type of elements of both arrays.
     * @param target The target array where the elements are written to.
     * @param source The source array where the elements are read from.
     * @param indices The list of indices to skip.
     * @note If the source or target is not effective, nothing happens
     * @note If the list of indices is not effective, the array is copied
     * entirely.
     * @note If the target array is not large enough, elements are copied until
     * the end is reached.
     * @note Indices in the list of indices that are out of range or not
     * effective are ignored.
     * @pre If the list of indices is effective, it must be ordered.
     */
    public static <T> void copySkipArray(T[] target, T[] source, Integer... indices) {
        if (indices != null) {
            copySkipArray(target, source, new ListArray<>(indices));
        } else {
            copySkipArray(target, source, (Iterable<Integer>) null);
        }
    }

    /**
     * Copy the content of the source array to the target array, but skip the
     * given indices.
     *
     * @param <T> The type of elements of both arrays.
     * @param target The target array where the elements are written to.
     * @param source The source array where the elements are read from.
     * @param indices The iterable of indices to skip.
     * @note If the source or target is not effective, nothing happens
     * @note If the list of indices is not effective, the array is copied
     * entirely.
     * @note If the target array is not large enough, elements are copied until
     * the end is reached.
     * @note Indices in the list of indices that are out of range or not
     * effective are ignored.
     * @pre If the list of indices is effective, it must be ordered.
     */
    public static <T> void copySkipArray(T[] target, T[] source, Iterable<Integer> indices) {
        if (indices != null) {
            copySkipArray(target, source, indices.iterator());
        } else {
            copySkipArray(target, source, (Iterator<Integer>) null);
        }
    }

    /**
     * Copy the content of the source array to the target array, but skip the
     * given indices.
     *
     * @param <T> The type of elements of both arrays.
     * @param target The target array where the elements are written to.
     * @param source The source array where the elements are read from.
     * @param indices The iterator of indices to skip.
     * @note If the source or target is not effective, nothing happens
     * @note If the list of indices is not effective, the array is copied
     * entirely.
     * @note If the target array is not large enough, elements are copied until
     * the end is reached.
     * @note Indices in the list of indices that are out of range or not
     * effective are ignored.
     * @pre If the list of indices is effective, it must be ordered.
     */
    public static <T> void copySkipArray(T[] target, T[] source, Iterator<Integer> indices) {
        if (target != null && source != null) {
            int ns = source.length;
            int nt = target.length;
            if (indices != null) {
                int i = 0x00, j = 0x00, l;
                while (indices.hasNext()) {
                    Integer ki = indices.next();
                    if (ki != null) {
                        int k = ki;
                        if (k >= 0x00 && k < ns) {
                            l = k - j;
                            System.arraycopy(source, j, target, i, l);
                            j = k + 0x01;
                            i += l;
                        }
                    }
                }
                System.arraycopy(source, j, target, i, Math.min(ns - j, nt - i));
            } else {
                System.arraycopy(source, 0x00, target, 0x00, Math.min(ns, nt));
            }
        }
    }

    /**
     * Checks if all elements of the given array have the same length.
     *
     * @param <T> The type of the second order elements.
     * @param data An array with minimum two dimensions.
     * @return True if the given array is not effective, has a length of zero,
     * or all its elements are effective and have the same length.
     */
    public static <T> boolean isConsistent(T[]  
        ... data) {
        if (data != null) {
            int m = data.length;
            if (m > 0x00) {
                if (data[0x00] != null) {
                    int n = data[0x00].length;
                    for (int i = 0x01; i < m; i++) {
                        if (data[i] == null || data[i].length != n) {
                            return false;
                        }
                    }
                } else {
                    return false;
                }
            }
        }
        return true;
    }
    
    /**
     * Fill the given array with the given value.
     * @param <T> The type of the elements of the array.
     * @param value The given value to fill the array with.
     * @param array The given array to fill.
     * @return The given array, after processing, can be used for chaining.
     * @note If the array is not effective, no action is performed.
     */
    public static <T> T[] fill (T value, T[] array) {
        return fill(value,0x00,array.length,array);
    }
    
    /**
     * Fill the given array with the given value.
     * @param <T> The type of the elements of the array.
     * @param value The given value to fill the array with.
     * @param array The given array to fill.
     * @param frm The index at which filling begins (inclusive.
     * @return The given array, after processing, can be used for chaining.
     * @note If the array is not effective, no action is performed.
     * @note The bounds are automatically modified if they are out of bounds.
     */
    public static <T> T[] fill (T value, int frm, T[] array) {
        if(array != null) {
            fill(value,frm,array.length,array);
        }
        return array;
    }
    
    /**
     * Fill the given array with the given value.
     * @param <T> The type of the elements of the array.
     * @param value The given value to fill the array with.
     * @param array The given array to fill.
     * @param frm The index at which filling begins (inclusive.
     * @param to The index at which filling ends (exclusive).
     * @return The given array, after processing, can be used for chaining.
     * @note If the array is not effective, no action is performed.
     * @note The bounds are automatically modified if they are out of bounds.
     */
    public static <T> T[] fill (T value, int frm, int to, T[] array) {
        if(array != null) {
            int i0 = Math.max(0x00,frm);
            int i1 = Math.min(to,array.length);
            for(int i = i0; i < i1; i++) {
                array[i] = value;
            }
        }
        return array;
    }

    private ArrayUtils() {
    }

}
