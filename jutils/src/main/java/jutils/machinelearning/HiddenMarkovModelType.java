package jutils.machinelearning;

/**
 * An enumeration describing the possible hidden Markov model topologies.
 * @author kommusoft
 */
public enum HiddenMarkovModelType {
    
    /**
     * A fully connected model, in which all states are reachable from all other
     * states.
     */
    Ergodic,
    
    /**
     * A model in which only forward state transitions are allowed.
     */
    Forward,
    
}
