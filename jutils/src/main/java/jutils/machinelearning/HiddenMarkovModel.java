package jutils.machinelearning;

/**
 *
 * @author kommusoft
 */
interface HiddenMarkovModel {

    double evaluate(int[] observations);

    double evaluate(int[] observations, boolean logarithm);

    int getNumberOfStates();

    int getNumberOfSymbols();

    double learn(int[] observations, double tolerance);

    double learn(int[] observations, int iterations);

    double learn(int[] observations, int iterations, double tolerance);

    double learn(int[][] observations, double tolerance);

    double learn(int[][] observations, int iterations);

    double learn(int[][] observations, int iterations, double tolerance);
    
}
