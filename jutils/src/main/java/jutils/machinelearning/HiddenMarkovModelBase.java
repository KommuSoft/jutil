package jutils.machinelearning;

/**
 *
 * @author kommusoft
 */
public class HiddenMarkovModelBase implements HiddenMarkovModel {

    private double[][] b; // Emission probabilities
    private final double[][] a; // Transition probabilities
    private final double[] pi; // Initial state probabilities

    public HiddenMarkovModelBase(int symbols, double[] probabilities) {
        this(null, probabilities, -0x01, HiddenMarkovModelType.Ergodic);
        if (symbols <= 0) {
            throw new IllegalArgumentException("symbols");
        }

        int states = this.getNumberOfStates();
        double[][] bnew = new double[states][symbols];
        double bij = 1.0d / symbols;// Initialize B with uniform probabilities
        for (int i = 0; i < states; i++) {
            double[] bi = new double[symbols];
            for (int j = 0; j < symbols; j++) {
                bi[j] = 1.0 / symbols;
            }
            bnew[i] = bi;
        }
        this.b = bnew;
    }

    public HiddenMarkovModelBase(double[][] transitions, double[][] emissions, double[] probabilities) {
        this(transitions, probabilities, -0x01, HiddenMarkovModelType.Ergodic);
        this.b = emissions;
    }

    public HiddenMarkovModelBase(int numberOfSymbols, int numberOfStates) {
        this(numberOfSymbols, numberOfStates, HiddenMarkovModelType.Ergodic);
    }

    public HiddenMarkovModelBase(int numberOfSymbols, int numberOfStates, HiddenMarkovModelType type) {
        this(null, null, numberOfStates, type);
        double[][] bnew = new double[numberOfStates][numberOfSymbols];
        double[] bnewi;
        double bij = 1.0d / numberOfSymbols;
        for (int i = 0x00; i < numberOfStates; i++) {
            bnewi = new double[numberOfSymbols];
            for (int j = 0x00; j < numberOfSymbols; j++) {
                bnewi[j] = bij;
            }
            bnew[i] = bnewi;
        }
        this.b = bnew;
    }

    private HiddenMarkovModelBase(double[][] a, double[] pi, int numberOfStates, HiddenMarkovModelType type) {
        int n = numberOfStates;
        if (a != null) {
            this.a = a;
        } else {
            double[][] anew = new double[n][n];
            double aij;
            switch (type) {
                case Ergodic:
                    aij = 1.0d / n;
                    for (int i = 0x00; i < n; i++) {
                        double[] ai = new double[n];
                        for (int j = 0x00; j < n; j++) {
                            ai[j] = aij;
                        }
                        anew[i] = ai;
                    }
                    break;
                case Forward:
                    for (int i = 0x00; i < n; i++) {
                        double[] ai = new double[n];
                        aij = 1.0d / (n - i);
                        for (int j = i; j < n; j++) {
                            ai[j] = aij;
                        }
                        anew[i] = ai;
                    }
                    break;
                default:
                    throw new IllegalArgumentException("Unknown hidden Markov model topology.");
            }
            this.a = anew;
        }
        if (pi != null) {
            this.pi = pi;
        } else {
            double[] pinew = new double[n];
            double pii = 1.0d / n;
            pinew[0x00] = 1.0d;
            this.pi = pinew;
        }
    }

    public int getNumberOfStates() {
        return a.length;
    }

    public int getNumberOfSymbols() {
        return b[0x00].length;
    }

    public double learn(int[] observations, int iterations, double tolerance) {
        return learn(new int[][]{observations}, iterations, tolerance);
    }

    public double learn(int[] observations, int iterations) {
        return learn(observations, iterations, 0.0);
    }

    public double learn(int[] observations, double tolerance) {
        return learn(observations, 0, tolerance);
    }

    public double learn(int[][] observations, double tolerance) {
        return learn(observations, 0, tolerance);
    }

    public double learn(int[][] observations, int iterations) {
        return learn(observations, iterations, 0);
    }

    public double learn(int[][] observations, int iterations, double tolerance) {
        if (iterations <= 0 && tolerance <= 0) {
            throw new IllegalArgumentException("Iterations and limit cannot be both zero.");
        }
        int S = this.getNumberOfStates();
        int O = this.getNumberOfSymbols();
        int N = observations.length;
        int currentIteration = 1;
        boolean stop = false;

        double[] pi = this.pi;
        double[][] a = this.a;
        double[][] b = this.b;

        double[][][][] epsilon = new double[N][][][]; // also referred as ksi or psi
        double[][][] gamma = new double[N][][];

        int maxT = 0x00;
        for (int i = 0; i < N; i++) {
            int T = observations[i].length;
            maxT = Math.max(maxT, T);
            epsilon[i] = new double[T][S][S];
            gamma[i] = new double[T][S];
        }

        double[] scaling = new double[maxT];
        double[][] fwd = new double[maxT][S], bwd = new double[maxT][S];

        double oldLikelihood = Double.MIN_VALUE;
        double newLikelihood = 0;

        do { // Until convergence or max iterations is reached
            for (int i = 0; i < N; i++) {// For each sequence in the observations input
                int[] sequence = observations[i];
                int T = sequence.length;
                // 1st step - Calculating the forward probability and the
                //            backward probability for each HMM state.
                forward(sequence, fwd, scaling);
                backward(sequence, bwd, scaling);

                // 2nd step - Determining the frequency of the transition-emission pair values
                //            and dividing it by the probability of the entire string.
                // Calculate gamma values for next computations
                for (int t = 0; t < T; t++) {
                    double s = 0.0d, sk;
                    for (int k = 0; k < S; k++) {
                        sk = fwd[t][k] * bwd[t][k];
                        gamma[i][t][k] = sk;
                        s += sk;
                    }
                    if (s > 0.0d) { // Scaling
                        double sinv = 1.0d / s;
                        for (int k = 0; k < S; k++) {
                            gamma[i][t][k] *= sinv;
                        }
                    }
                }

                // Calculate epsilon values for next computations
                for (int t = 0; t < T - 1; t++) {
                    double s = 0.0d;

                    for (int k = 0; k < S; k++) {
                        for (int l = 0; l < S; l++) {
                            s += epsilon[i][t][k][l] = fwd[t][k] * a[k][l] * bwd[t + 1][l] * b[l][sequence[t + 1]];
                        }
                    }

                    if (s > 0.0d) { // Scaling
                        double sinv = 1.0d / s;
                        for (int k = 0; k < S; k++) {
                            for (int l = 0; l < S; l++) {
                                epsilon[i][t][k][l] *= sinv;

                            }
                        }
                    }
                }

                // Compute log-likelihood for the given sequence
                for (int t = 0; t < T; t++) {
                    newLikelihood += Math.log(scaling[t]);
                }
            }

            // Average the likelihood for all sequences
            newLikelihood /= N;

            // Check if the model has converged or we should stop
            if (checkConvergence(oldLikelihood, newLikelihood, currentIteration, iterations, tolerance)) {
                stop = true;
            } else {
                // 3. Continue with parameter re-estimation
                currentIteration++;
                oldLikelihood = newLikelihood;
                newLikelihood = 0.0;

                // 3.1 Re-estimation of initial state probabilities 
                for (int k = 0; k < S; k++) {
                    double sum = 0;
                    for (int i = 0; i < N; i++) {
                        sum += gamma[i][0][k];
                    }
                    pi[k] = sum / N;
                }

                // 3.2 Re-estimation of transition probabilities 
                for (int i = 0; i < S; i++) {
                    for (int j = 0; j < S; j++) {
                        double den = 0, num = 0;

                        for (int k = 0; k < N; k++) {
                            int T = observations[k].length;

                            for (int l = 0; l < T - 1; l++) {
                                num += epsilon[k][l][i][j];
                            }

                            for (int l = 0; l < T - 1; l++) {
                                den += gamma[k][l][i];
                            }
                        }
                        a[i][j] = (den != 0) ? num / den : 0.0;
                    }
                }

                // 3.3 Re-estimation of emission probabilities
                for (int i = 0; i < S; i++) {
                    for (int j = 0; j < O; j++) {
                        double den = 0, num = 0;

                        for (int k = 0; k < N; k++) {
                            int T = observations[k].length;

                            for (int l = 0; l < T; l++) {
                                if (observations[k][l] == j) {
                                    num += gamma[k][l][i];
                                }
                            }

                            for (int l = 0; l < T; l++) {
                                den += gamma[k][l][i];
                            }
                        }
                        // avoid locking a parameter in zero.
                        b[i][j] = (num <= 0) ? 1e-10 : num / den;
                    }
                }

            }

        } while (!stop);
        return newLikelihood;
    }

    public double evaluate(int[] observations) {
        return evaluate(observations, false);
    }

    public double evaluate(int[] observations, boolean logarithm) {
        if (observations == null) {
            throw new NullPointerException("observations");
        }

        if (observations.length <= 0) {
            return 0.0;
        }

        // Forward algorithm
        int T = observations.length;
        int S = this.getNumberOfStates();
        double likelihood = 0.0d;
        double[][] fw = new double[T][S];
        double[] coefficients = new double[T];

        // Compute forward probabilities
        forward(observations, fw, coefficients);

        for (int i = 0; i < T; i++) {
            likelihood += Math.log(coefficients[i]);
        }

        // Return the sequence probability
        return logarithm ? likelihood : Math.exp(likelihood);
    }

    private void forward(int[] sequence, double[][] fwd, double[] scaling) {
        int T = sequence.length;
        int S = this.getNumberOfStates();
        double[] pi = this.pi;
        double[][] a = this.a;
        double[][] b = this.b;

        // 1. Initialization
        scaling[0x00] = 0.0d;
        for (int i = 0; i < S; i++) {
            double fwdi = pi[i] * b[i][sequence[0]];
            fwd[0][i] = fwdi;
            scaling[0] += fwdi;
        }
        if (scaling[0] > 0.0) { // Scaling
            double sc = 1.0d / scaling[0x00];
            for (int i = 0; i < S; i++) {
                fwd[0][i] *= sc;
            }
        }
        // 2. Induction
        for (int t = 1; t < T; t++) {
            scaling[t] = 0.0d;
            for (int i = 0; i < S; i++) {
                double p = b[i][sequence[t]];
                double sum = 0.0;
                for (int j = 0; j < S; j++) {
                    sum += fwd[t - 1][j] * a[j][i];
                }
                sum *= p;
                fwd[t][i] = sum;
                scaling[t] += sum;// scaling coefficient
            }
            if (scaling[t] > 0.0) { // Scaling
                double sc = 1.0d / scaling[t];
                for (int i = 0; i < S; i++) {
                    fwd[t][i] *= sc;
                }
            }
        }
    }

    private void backward(int[] sequence, double[][] bwd, double[] scaling) {
        int T = sequence.length;
        int S = this.getNumberOfStates();
        double[] pi = this.pi;
        double[][] a = this.a;
        double[][] b = this.b;

        // 1. Initialization
        double sc = 1.0d / scaling[T - 1];
        for (int i = 0; i < S; i++) {
            bwd[T - 1][i] = sc;
        }

        // 2. Induction
        for (int t = T - 2; t >= 0; t--) {
            sc = 1.0d / scaling[t];
            for (int i = 0; i < S; i++) {
                double sum = 0;
                for (int j = 0; j < S; j++) {
                    sum += a[i][j] * b[j][sequence[t + 1]] * bwd[t + 1][j];
                }
                bwd[t][i] += sum * sc;
            }
        }
    }

    private boolean checkConvergence(double oldLikelihood, double newLikelihood, int currentIteration, int maxIterations, double tolerance) {
        return ((tolerance > 0.0d && (Math.abs(oldLikelihood - newLikelihood) <= tolerance || (maxIterations > 0 && currentIteration >= maxIterations))) || (tolerance <= 0.0d && currentIteration == maxIterations) || Double.isNaN(newLikelihood) || Double.isInfinite(newLikelihood));// Stopping criteria is likelihood convergence or number of iterations
    }

}
