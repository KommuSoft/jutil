package jutils;

/**
 *
 * @author kommusoft
 * @param <TTag> The type of the tag.
 */
public interface Tagable<TTag> {

    public abstract TTag getTag();

}
