package jutils.designpatterns;

/**
 *
 * @author kommusoft
 * @param <TRoot> The root type of the composite pattern.
 * @param <TNode> The type of the branching node (with children) of the
 * composite pattern.
 */
public interface CompositeLeaf<TRoot extends CompositeComponent<TRoot, TNode>, TNode extends CompositeNode<TRoot, TNode>> extends CompositeComponent<TRoot, TNode> {

}
