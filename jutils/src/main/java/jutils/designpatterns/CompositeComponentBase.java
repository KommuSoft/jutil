package jutils.designpatterns;

import java.util.logging.Logger;

/**
 *
 * @author kommusoft
 * @param <TRoot> The root type of the composite pattern.
 * @param <TNode> The type of the branching node (with children) of the
 * composite pattern.
 */
public class CompositeComponentBase<TRoot extends CompositeComponent<TRoot, TNode>, TNode extends CompositeNode<TRoot, TNode>> implements CompositeComponent<TRoot, TNode> {

    private static final Logger LOG = Logger.getLogger(CompositeComponentBase.class.getName());

    private final TNode root;

    public CompositeComponentBase(TNode root) {
        this.root = root;
    }

    public CompositeComponentBase() {
        this(null);
    }

    @Override
    public TNode getParent() {
        return this.root;
    }

}
