package jutils.designpatterns;

import java.util.Collection;
import java.util.Collections;
import java.util.logging.Logger;

/**
 *
 * @author kommusoft
 * @param <TRoot> The root type of the composite pattern.
 */
public class MutableCompositeCollectionNodeBase<TRoot extends CompositeComponent<TRoot, TNode>, TNode extends CompositeNode<TRoot, TNode>> extends CompositeComponentBase<TRoot, TNode> implements MutableCompositeNode<TRoot, TNode> {

    private static final Logger LOG = Logger.getLogger(MutableCompositeCollectionNodeBase.class.getName());

    private final Collection<TRoot> innerCollection;

    public MutableCompositeCollectionNodeBase(TNode parent, Collection<TRoot> innerCollection) {
        super(parent);
        this.innerCollection = innerCollection;
    }

    public MutableCompositeCollectionNodeBase(Collection<TRoot> innerCollection) {
        super();
        this.innerCollection = innerCollection;
    }

    @Override
    public void addChild(TRoot child) {
        this.innerCollection.add(child);
    }

    @Override
    public void removeChild(TRoot child) {
        this.innerCollection.remove(child);
    }

    @Override
    public Iterable<? extends TRoot> getChildren() {
        return Collections.unmodifiableCollection(this.innerCollection);
    }

}
