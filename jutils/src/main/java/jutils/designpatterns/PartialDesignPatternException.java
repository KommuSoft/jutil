package jutils.designpatterns;

import javax.naming.OperationNotSupportedException;

/**
 * An exception to warn the user the design pattern is only partially
 * implemented.
 *
 * @author kommusoft
 */
public class PartialDesignPatternException extends OperationNotSupportedException {

    /**
     * Creates a new instance of <code>PartialDesignPatternException</code>
     * without detail message.
     */
    public PartialDesignPatternException() {
    }

    /**
     * Constructs an instance of <code>PartialDesignPatternException</code> with
     * the specified detail message.
     *
     * @param msg the detail message.
     */
    public PartialDesignPatternException(String msg) {
        super(msg);
    }
}
