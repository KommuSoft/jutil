package jutils.designpatterns.draw;

import java.io.IOException;
import java.io.Writer;
import java.util.Stack;
import java.util.logging.Logger;
import jutils.designpatterns.CompositeComponent;
import jutils.designpatterns.CompositeNode;
import jutils.draw.DotGraphDrawerBase;
import jutils.algebra.Function;
import jutils.algebra.functions.FunctionToString;

/**
 *
 * @author kommusoft
 * @param <TRoot> The type of the root of the composite pattern.
 * @param <TNode> The type of the branching nodes of the composite pattern.
 */
public class CompositePatternDotDrawer<TRoot extends CompositeComponent<TRoot, TNode>, TNode extends CompositeNode<TRoot, TNode>> extends DotGraphDrawerBase<TRoot> {

    private static final Logger LOG = Logger.getLogger(CompositePatternDotDrawer.class.getName());

    private final Function<? super TRoot, String> toTextual;

    public CompositePatternDotDrawer() {
        this(new FunctionToString<>());
    }

    public CompositePatternDotDrawer(Function<? super TRoot, String> toTextual) {
        this.toTextual = toTextual;
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void innerWrite(TRoot input, Writer writer) throws IOException {
        this.nodeStatement(writer, toTextual.evaluate(input));
        Stack<TRoot> stack = new Stack<>();
        this.expand(stack, input);
        TRoot current, parent;
        while (!stack.isEmpty()) {
            current = stack.pop();
            parent = (TRoot) current.getParent();
            this.edgeStatement(writer, toTextual.evaluate(parent), toTextual.evaluate(current));
            this.expand(stack, current);
        }
    }

    private void expand(Stack<TRoot> stack, TRoot current) {
        try {
            CompositeNode<TRoot, TNode> node = (CompositeNode<TRoot, TNode>) current;
            for (TRoot child : node.getChildren()) {
                stack.push(child);
            }
        } catch (Throwable t) {
        }
    }

}
