/**
 * Provides a number of classes only used by tests (for instance storing
 * temporary information, or provoke certain behavior).
 */
package jutils.foo;
