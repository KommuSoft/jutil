package jutils.machinelearning;

import junit.framework.Assert;
import junit.framework.TestCase;

/**
 *
 * @author kommusoft
 */
public class HiddenMarkovModelBaseTest extends TestCase {

    int[][] sequences = new int[][]{
        new int[]{0, 1, 1, 1, 1, 0, 1, 1, 1, 1},
        new int[]{0, 1, 1, 1, 0, 1, 1, 1, 1, 1},
        new int[]{0, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        new int[]{0, 1, 1, 1, 1, 1},
        new int[]{0, 1, 1, 1, 1, 1, 1},
        new int[]{0, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        new int[]{0, 1, 1, 1, 1, 1, 1, 1, 1, 1}};

    public HiddenMarkovModelBaseTest(String testName) {
        super(testName);
    }

    /**
     * Test of getNumberOfStates method, of class HiddenMarkovModelBase.
     */
    public void testGetNumberOfStates() {
        HiddenMarkovModelBase hmm = new HiddenMarkovModelBase(2, 3);
        Assert.assertEquals(3, hmm.getNumberOfStates());
    }

    /**
     * Test of getNumberOfSymbols method, of class HiddenMarkovModelBase.
     */
    public void testGetNumberOfSymbols() {
        HiddenMarkovModelBase hmm = new HiddenMarkovModelBase(2, 3);
        Assert.assertEquals(2, hmm.getNumberOfSymbols());
    }

    /**
     * Test of learn method, of class HiddenMarkovModelBase.
     */
    public void testLearn() {
        HiddenMarkovModelBase hmm = new HiddenMarkovModelBase(2, 3);
        hmm.learn(sequences, 0.0001);
        double l1 = hmm.evaluate(new int[]{0, 1});      // 0.9999
        Assert.assertEquals(0.9999, l1, 1e-4d);
        double l2 = hmm.evaluate(new int[]{0, 1, 1, 1});  // 0.9166
        Assert.assertEquals(0.9166, l2, 1e-4d);
        double l3 = hmm.evaluate(new int[]{1, 1});      // 0.0000
        Assert.assertEquals(0.0000, l3, 1e-4d);
        double l4 = hmm.evaluate(new int[]{1, 0, 0, 0});  // 0.0000
        Assert.assertEquals(0.0000, l4, 1e-4d);
        double l5 = hmm.evaluate(new int[]{0, 1, 0, 1, 1, 1, 1, 1, 1}); // 0.0342
        Assert.assertEquals(0.0342, l5, 1e-4d);
        double l6 = hmm.evaluate(new int[]{0, 1, 1, 1, 1, 1, 1, 0, 1}); // 0.0342
        Assert.assertEquals(0.0342, l6, 1e-4d);
    }

}
