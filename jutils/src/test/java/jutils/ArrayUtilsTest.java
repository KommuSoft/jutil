package jutils;

import java.util.Arrays;
import junit.framework.Assert;
import junit.framework.TestCase;

/**
 *
 * @author kommusoft
 */
public class ArrayUtilsTest extends TestCase {

    public ArrayUtilsTest() {
    }

    /**
     * Test of copySkipArray method, of class ArrayUtils.
     */
    public void testCopySkipArray_3args_1_00() {
        Object[] target = null;
        Object[] source = null;
        int[] indices = null;
        ArrayUtils.copySkipArray(target, source, indices);
        Assert.assertNull(source);
        Assert.assertNull(target);
        Assert.assertNull(indices);
    }

    /**
     * Test of copySkipArray method, of class ArrayUtils.
     */
    public void testCopySkipArray_3args_1_01() {
        Integer[] target = new Integer[0x05];
        Integer[] source = new Integer[]{2, 3, 5, 8, 13, 21};
        int[] indices = null;
        ArrayUtils.copySkipArray(target, source, indices);
        assertTrue(Arrays.equals(new Integer[]{2, 3, 5, 8, 13}, target));
        assertTrue(Arrays.equals(new Integer[]{2, 3, 5, 8, 13, 21}, source));
        Assert.assertNull(indices);
    }

    /**
     * Test of copySkipArray method, of class ArrayUtils.
     */
    public void testCopySkipArray_3args_1_02() {
        Integer[] target = new Integer[0x05];
        Integer[] source = new Integer[]{2, 3, 5, 8, 13, 21};
        int[] indices = new int[]{};
        ArrayUtils.copySkipArray(target, source, indices);
        assertTrue(Arrays.equals(new Integer[]{2, 3, 5, 8, 13}, target));
        assertTrue(Arrays.equals(new Integer[]{2, 3, 5, 8, 13, 21}, source));
        assertTrue(Arrays.equals(new int[]{}, indices));
    }

    /**
     * Test of copySkipArray method, of class ArrayUtils.
     */
    public void testCopySkipArray_3args_1_03() {
        Integer[] target = new Integer[0x05];
        Integer[] source = new Integer[]{2, 3, 5, 8, 13, 21};
        int[] indices = new int[]{0x00};
        ArrayUtils.copySkipArray(target, source, indices);
        assertTrue(Arrays.equals(new Integer[]{3, 5, 8, 13, 21}, target));
        assertTrue(Arrays.equals(new Integer[]{2, 3, 5, 8, 13, 21}, source));
        assertTrue(Arrays.equals(new int[]{0x00}, indices));
    }

    /**
     * Test of copySkipArray method, of class ArrayUtils.
     */
    public void testCopySkipArray_3args_1_04() {
        Integer[] target = new Integer[0x04];
        Integer[] source = new Integer[]{2, 3, 5, 8, 13, 21};
        int[] indices = new int[]{0x00};
        ArrayUtils.copySkipArray(target, source, indices);
        assertTrue(Arrays.equals(new Integer[]{3, 5, 8, 13}, target));
        assertTrue(Arrays.equals(new Integer[]{2, 3, 5, 8, 13, 21}, source));
        assertTrue(Arrays.equals(new int[]{0x00}, indices));
    }

    /**
     * Test of copySkipArray method, of class ArrayUtils.
     */
    public void testCopySkipArray_3args_1_05() {
        Integer[] target = new Integer[0x05];
        Integer[] source = new Integer[]{2, 3, 5, 8, 13, 21};
        int[] indices = new int[]{0x01};
        ArrayUtils.copySkipArray(target, source, indices);
        assertTrue(Arrays.equals(new Integer[]{2, 5, 8, 13, 21}, target));
        assertTrue(Arrays.equals(new Integer[]{2, 3, 5, 8, 13, 21}, source));
        assertTrue(Arrays.equals(new int[]{0x01}, indices));
    }

    /**
     * Test of copySkipArray method, of class ArrayUtils.
     */
    public void testCopySkipArray_3args_1_06() {
        Integer[] target = new Integer[0x04];
        Integer[] source = new Integer[]{2, 3, 5, 8, 13, 21};
        int[] indices = new int[]{0x01};
        ArrayUtils.copySkipArray(target, source, indices);
        assertTrue(Arrays.equals(new Integer[]{2, 5, 8, 13}, target));
        assertTrue(Arrays.equals(new Integer[]{2, 3, 5, 8, 13, 21}, source));
        assertTrue(Arrays.equals(new int[]{0x01}, indices));
    }

    /**
     * Test of copySkipArray method, of class ArrayUtils.
     */
    public void testCopySkipArray_3args_1_07() {
        Integer[] target = new Integer[0x05];
        Integer[] source = new Integer[]{2, 3, 5, 8, 13, 21};
        int[] indices = new int[]{0x01, 0x03};
        ArrayUtils.copySkipArray(target, source, indices);
        assertTrue(Arrays.equals(new Integer[]{2, 5, 13, 21, null}, target));
        assertTrue(Arrays.equals(new Integer[]{2, 3, 5, 8, 13, 21}, source));
        assertTrue(Arrays.equals(new int[]{0x01, 0x03}, indices));
    }

    /**
     * Test of copySkipArray method, of class ArrayUtils.
     */
    public void testCopySkipArray_3args_1_08() {
        Integer[] target = new Integer[0x04];
        Integer[] source = new Integer[]{2, 3, 5, 8, 13, 21};
        int[] indices = new int[]{0x01, 0x03};
        ArrayUtils.copySkipArray(target, source, indices);
        assertTrue(Arrays.equals(new Integer[]{2, 5, 13, 21}, target));
        assertTrue(Arrays.equals(new Integer[]{2, 3, 5, 8, 13, 21}, source));
        assertTrue(Arrays.equals(new int[]{0x01, 0x03}, indices));
    }

    /**
     * Test of copySkipArray method, of class ArrayUtils.
     */
    public void testCopySkipArray_3args_1_09() {
        Integer[] target = new Integer[0x03];
        Integer[] source = new Integer[]{2, 3, 5, 8, 13, 21};
        int[] indices = new int[]{0x01, 0x03};
        ArrayUtils.copySkipArray(target, source, indices);
        assertTrue(Arrays.equals(new Integer[]{2, 5, 13}, target));
        assertTrue(Arrays.equals(new Integer[]{2, 3, 5, 8, 13, 21}, source));
        assertTrue(Arrays.equals(new int[]{0x01, 0x03}, indices));
    }

}
