package jutils.designpatterns.draw;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.logging.Logger;
import junit.framework.Assert;
import junit.framework.TestCase;
import jutils.designpatterns.CompositeComponentBase;
import jutils.designpatterns.CompositeNode;
import jutils.designpatterns.MutableCompositeNode;
import jutils.lists.ListArray;

/**
 *
 * @author kommusoft
 */
public class CompositePatternDotDrawerTest extends TestCase {

    private static final Logger LOG = Logger.getLogger(CompositePatternDotDrawerTest.class.getName());

    public CompositePatternDotDrawerTest() {
    }

    
    public void testInnerWrite() throws Exception {
        FooBar leafar = new FooBar("abcdefghijklmnopqr");
        FooBar leafai = new FooBar(leafar, "abcdefghi");
        FooBar leafjr = new FooBar(leafar, "jklmnopqr");
        FooBar leafac = new FooBar(leafai, "abc");
        FooBar leafde = new FooBar(leafai, "de");
        FooBar leaffi = new FooBar(leafai, "fghi");
        FooBar leafjl = new FooBar(leafjr, "jkl");
        FooBar leafmr = new FooBar(leafjr, "mnopqr");
        Foo leafa = new Foo(leafac, "a");
        Foo leafb = new Foo(leafac, "b");
        Foo leafc = new Foo(leafac, "c");
        leafac.addChildren(leafa, leafb, leafc);
        Foo leafd = new Foo(leafde, "d");
        Foo leafe = new Foo(leafde, "e");
        leafde.addChildren(leafd, leafe);
        Foo leaff = new Foo(leaffi, "f");
        Foo leafg = new Foo(leaffi, "g");
        Foo leafh = new Foo(leaffi, "h");
        Foo leafi = new Foo(leaffi, "i");
        leaffi.addChildren(leaff, leafg, leafh, leafi);
        leafai.addChildren(leafac, leafde, leaffi);
        Foo leafj = new Foo(leafjl, "j");
        Foo leafk = new Foo(leafjl, "k");
        Foo leafl = new Foo(leafjl, "l");
        leafjl.addChildren(leafj, leafk, leafl);
        Foo leafm = new Foo(leafmr, "m");
        Foo leafn = new Foo(leafmr, "n");
        Foo leafo = new Foo(leafmr, "o");
        Foo leafp = new Foo(leafmr, "p");
        Foo leafq = new Foo(leafmr, "q");
        Foo leafr = new Foo(leafmr, "r");
        leafmr.addChildren(leafm, leafn, leafo, leafp, leafq, leafr);
        leafjr.addChildren(leafjl, leafmr);
        leafar.addChildren(leafai, leafjr);
        CompositePatternDotDrawer<Foo, FooBar> cpdd = new CompositePatternDotDrawer<>();
        String result = cpdd.write(leafar);
        String expected = "digraph {\n"
                + "	\"abcdefghijklmnopqr\";\n"
                + "	\"abcdefghijklmnopqr\" -> \"jklmnopqr\";\n"
                + "	\"jklmnopqr\" -> \"mnopqr\";\n"
                + "	\"mnopqr\" -> \"r\";\n"
                + "	\"mnopqr\" -> \"q\";\n"
                + "	\"mnopqr\" -> \"p\";\n"
                + "	\"mnopqr\" -> \"o\";\n"
                + "	\"mnopqr\" -> \"n\";\n"
                + "	\"mnopqr\" -> \"m\";\n"
                + "	\"jklmnopqr\" -> \"jkl\";\n"
                + "	\"jkl\" -> \"l\";\n"
                + "	\"jkl\" -> \"k\";\n"
                + "	\"jkl\" -> \"j\";\n"
                + "	\"abcdefghijklmnopqr\" -> \"abcdefghi\";\n"
                + "	\"abcdefghi\" -> \"fghi\";\n"
                + "	\"fghi\" -> \"i\";\n"
                + "	\"fghi\" -> \"h\";\n"
                + "	\"fghi\" -> \"g\";\n"
                + "	\"fghi\" -> \"f\";\n"
                + "	\"abcdefghi\" -> \"de\";\n"
                + "	\"de\" -> \"e\";\n"
                + "	\"de\" -> \"d\";\n"
                + "	\"abcdefghi\" -> \"abc\";\n"
                + "	\"abc\" -> \"c\";\n"
                + "	\"abc\" -> \"b\";\n"
                + "	\"abc\" -> \"a\";\n"
                + "}\n";
        Assert.assertEquals(expected, result);
    }

    public class Foo extends CompositeComponentBase<Foo, FooBar> {

        private final String name;

        public Foo(FooBar parent, String name) {
            super(parent);
            this.name = name;
        }

        public Foo(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return String.format("\"%s\"", this.name);
        }

    }

    public class FooBar extends Foo implements MutableCompositeNode<Foo, FooBar>, CompositeNode<Foo, FooBar> {

        private final Collection<Foo> innerCollection;

        public FooBar(String name, Collection<Foo> children) {
            this(name);
            this.addChildren(children);
        }

        public FooBar(FooBar parent, String name, Collection<Foo> children) {
            this(parent, name);
            this.addChildren(children);
        }

        public FooBar(FooBar parent, String name) {
            super(parent, name);
            this.innerCollection = new ArrayList<Foo>();
        }

        public FooBar(String name) {
            super(name);
            this.innerCollection = new ArrayList<Foo>();
        }

        public FooBar(FooBar parent, String name, Foo... elements) {
            this(parent, name);
            this.addChildren(elements);
        }

        public FooBar(String name, Foo... elements) {
            this(name);
            this.addChildren(elements);
        }

        @Override
        public Iterable<Foo> getChildren() {
            return Collections.unmodifiableCollection(this.innerCollection);
        }

        @Override
        public void addChild(Foo child) {
            this.innerCollection.add(child);
        }

        public void addChildren(Collection<Foo> children) {
            this.innerCollection.addAll(children);
        }

        public void addChildren(Foo... children) {
            this.addChildren(new ListArray<>(children));
        }

        @Override
        public void removeChild(Foo child) {
            this.innerCollection.remove(child);
        }

    }

}
