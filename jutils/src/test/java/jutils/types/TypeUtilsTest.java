package jutils.types;

import java.util.Set;
import junit.framework.Assert;
import junit.framework.TestCase;
import jutils.collections.valuesets.BooleanSet;
import jutils.collections.valuesets.ByteSet;
import jutils.collections.valuesets.CharacterSet;
import jutils.foo.FooEnum;
import jutils.testing.AssertExtensions;

/**
 *
 * @author kommusoft
 */
public class TypeUtilsTest extends TestCase {

    public TypeUtilsTest() {
    }

    /**
     * Test of isNominal method, of class TypeUtils.
     */
    public void testIsNominal00() {
        Assert.assertFalse(TypeUtils.isNominal(null));
    }

    /**
     * Test of isNominal method, of class TypeUtils.
     */
    public void testIsNominal01() {
        Assert.assertTrue(TypeUtils.isNominal(boolean.class));
    }

    /**
     * Test of isNominal method, of class TypeUtils.
     */
    public void testIsNominal02() {
        Assert.assertTrue(TypeUtils.isNominal(char.class));
    }

    /**
     * Test of isNominal method, of class TypeUtils.
     */
    public void testIsNominal03() {
        Assert.assertFalse(TypeUtils.isNominal(String.class));
    }

    /**
     * Test of isNominal method, of class TypeUtils.
     */
    public void testIsNominal04() {
        Assert.assertFalse(TypeUtils.isNominal(Object.class));
    }

    /**
     * Test of isNominal method, of class TypeUtils.
     */
    public void testIsNominal05() {
        Assert.assertFalse(TypeUtils.isNominal(int.class));
    }

    /**
     * Test of isNominal method, of class TypeUtils.
     */
    public void testIsNominal06() {
        Assert.assertFalse(TypeUtils.isNominal(long.class));
    }

    /**
     * Test of isNominal method, of class TypeUtils.
     */
    public void testIsNominal07() {
        Assert.assertFalse(TypeUtils.isNominal(short.class));
    }

    /**
     * Test of isNominal method, of class TypeUtils.
     */
    public void testIsNominal08() {
        Assert.assertFalse(TypeUtils.isNominal(int[].class));
    }

    /**
     * Test of isNominal method, of class TypeUtils.
     */
    public void testIsNominal09() {
        Assert.assertFalse(TypeUtils.isNominal(TypeUtilsTest.class));
    }

    /**
     * Test of isNominal method, of class TypeUtils.
     */
    public void testIsNominal10() {
        Assert.assertFalse(TypeUtils.isNominal(double.class));
    }

    /**
     * Test of isNominal method, of class TypeUtils.
     */
    public void testIsNominal11() {
        Assert.assertFalse(TypeUtils.isNominal(float.class));
    }

    /**
     * Test of isNominal method, of class TypeUtils.
     */
    public void testIsNominal12() {
        Assert.assertTrue(TypeUtils.isNominal(FooEnum.class));
    }

    /**
     * Test of isNominal method, of class TypeUtils.
     */
    public void testIsNominal13() {
        Assert.assertTrue(TypeUtils.isNominal(byte.class));
    }

    /**
     * Test of isNominal method, of class TypeUtils.
     */
    public void testIsNominal14() {
        Assert.assertTrue(TypeUtils.isNominal(Byte.class));
    }

    /**
     * Test of isBuiltinContinu method, of class TypeUtils.
     */
    public void testIsBuiltinContinu00() {
        Assert.assertFalse(TypeUtils.isBuiltinContinu(null));
    }

    /**
     * Test of isBuiltinContinu method, of class TypeUtils.
     */
    public void testIsBuiltinContinu01() {
        Assert.assertFalse(TypeUtils.isBuiltinContinu(boolean.class));
    }

    /**
     * Test of isBuiltinContinu method, of class TypeUtils.
     */
    public void testIsBuiltinContinu02() {
        Assert.assertFalse(TypeUtils.isBuiltinContinu(char.class));
    }

    /**
     * Test of isBuiltinContinu method, of class TypeUtils.
     */
    public void testIsBuiltinContinu03() {
        Assert.assertFalse(TypeUtils.isBuiltinContinu(String.class));
    }

    /**
     * Test of isBuiltinContinu method, of class TypeUtils.
     */
    public void testIsBuiltinContinu04() {
        Assert.assertFalse(TypeUtils.isBuiltinContinu(Object.class));
    }

    /**
     * Test of isBuiltinContinu method, of class TypeUtils.
     */
    public void testIsBuiltinContinu05() {
        Assert.assertFalse(TypeUtils.isBuiltinContinu(int.class));
    }

    /**
     * Test of isBuiltinContinu method, of class TypeUtils.
     */
    public void testIsBuiltinContinu06() {
        Assert.assertFalse(TypeUtils.isBuiltinContinu(long.class));
    }

    /**
     * Test of isBuiltinContinu method, of class TypeUtils.
     */
    public void testIsBuiltinContinu07() {
        Assert.assertFalse(TypeUtils.isBuiltinContinu(short.class));
    }

    /**
     * Test of isBuiltinContinu method, of class TypeUtils.
     */
    public void testIsBuiltinContinu08() {
        Assert.assertFalse(TypeUtils.isBuiltinContinu(int[].class));
    }

    /**
     * Test of isBuiltinContinu method, of class TypeUtils.
     */
    public void testIsBuiltinContinu09() {
        Assert.assertFalse(TypeUtils.isBuiltinContinu(TypeUtilsTest.class));
    }

    /**
     * Test of isBuiltinContinu method, of class TypeUtils.
     */
    public void testIsBuiltinContinu10() {
        Assert.assertTrue(TypeUtils.isBuiltinContinu(double.class));
    }

    /**
     * Test of isBuiltinContinu method, of class TypeUtils.
     */
    public void testIsBuiltinContinu11() {
        Assert.assertTrue(TypeUtils.isBuiltinContinu(float.class));
    }

    /**
     * Test of isOrdinal method, of class TypeUtils.
     */
    public void testIsOrdinal00() {
        Assert.assertFalse(TypeUtils.isOrdinal(null));
    }

    /**
     * Test of isOrdinal method, of class TypeUtils.
     */
    public void testIsOrdinal01() {
        Assert.assertTrue(TypeUtils.isOrdinal(boolean.class));
    }

    /**
     * Test of isOrdinal method, of class TypeUtils.
     */
    public void testIsOrdinal02() {
        Assert.assertTrue(TypeUtils.isOrdinal(char.class));
    }

    /**
     * Test of isOrdinal method, of class TypeUtils.
     */
    public void testIsOrdinal03() {
        Assert.assertTrue(TypeUtils.isOrdinal(String.class));
    }

    /**
     * Test of isOrdinal method, of class TypeUtils.
     */
    public void testIsOrdinal04() {
        Assert.assertFalse(TypeUtils.isOrdinal(Object.class));
    }

    /**
     * Test of isOrdinal method, of class TypeUtils.
     */
    public void testIsOrdinal05() {
        Assert.assertTrue(TypeUtils.isOrdinal(int.class));
    }

    /**
     * Test of isOrdinal method, of class TypeUtils.
     */
    public void testIsOrdinal06() {
        Assert.assertTrue(TypeUtils.isOrdinal(long.class));
    }

    /**
     * Test of isOrdinal method, of class TypeUtils.
     */
    public void testIsOrdinal07() {
        Assert.assertTrue(TypeUtils.isOrdinal(short.class));
    }

    /**
     * Test of isOrdinal method, of class TypeUtils.
     */
    public void testIsOrdinal08() {
        Assert.assertFalse(TypeUtils.isOrdinal(int[].class));
    }

    /**
     * Test of isOrdinal method, of class TypeUtils.
     */
    public void testIsOrdinal09() {
        Assert.assertFalse(TypeUtils.isOrdinal(TypeUtilsTest.class));
    }

    /**
     * Test of isOrdinal method, of class TypeUtils.
     */
    public void testIsOrdinal10() {
        Assert.assertTrue(TypeUtils.isOrdinal(double.class));
    }

    /**
     * Test of isOrdinal method, of class TypeUtils.
     */
    public void testIsOrdinal11() {
        Assert.assertTrue(TypeUtils.isOrdinal(float.class));
    }

    /**
     * Test of getNominalSet method, of class TypeUtils.
     */
    public void testGetNominalSet00() {
        Set result = TypeUtils.getNominalSet(null);
        Assert.assertNull(result);
    }

    /**
     * Test of getNominalSet method, of class TypeUtils.
     */
    public void testGetNominalSet01() {
        Set result = TypeUtils.getNominalSet(Boolean.class);
        Assert.assertNotNull(result);
        Assert.assertEquals(BooleanSet.Instance, result);
    }

    /**
     * Test of getNominalSet method, of class TypeUtils.
     */
    public void testGetNominalSet02() {
        Set result = TypeUtils.getNominalSet(boolean.class);
        Assert.assertNotNull(result);
        Assert.assertEquals(BooleanSet.Instance, result);
    }

    /**
     * Test of getNominalSet method, of class TypeUtils.
     */
    public void testGetNominalSet03() {
        Set result = TypeUtils.getNominalSet(char.class);
        Assert.assertNotNull(result);
        Assert.assertEquals(CharacterSet.Instance, result);
    }

    /**
     * Test of getNominalSet method, of class TypeUtils.
     */
    public void testGetNominalSet04() {
        Set result = TypeUtils.getNominalSet(Character.class);
        Assert.assertNotNull(result);
        Assert.assertEquals(CharacterSet.Instance, result);
    }

    /**
     * Test of getNominalSet method, of class TypeUtils.
     */
    public void testGetNominalSet05() {
        Set result = TypeUtils.getNominalSet(Object.class);
        Assert.assertNull(result);
    }

    /**
     * Test of getNominalSet method, of class TypeUtils.
     */
    public void testGetNominalSet06() {
        Set result = TypeUtils.getNominalSet(int.class);
        Assert.assertNull(result);
    }

    /**
     * Test of getNominalSet method, of class TypeUtils.
     */
    public void testGetNominalSet07() {
        Set result = TypeUtils.getNominalSet(Integer.class);
        Assert.assertNull(result);
    }

    /**
     * Test of getNominalSet method, of class TypeUtils.
     */
    public void testGetNominalSet08() {
        Set result = TypeUtils.getNominalSet(long.class);
        Assert.assertNull(result);
    }

    /**
     * Test of getNominalSet method, of class TypeUtils.
     */
    public void testGetNominalSet09() {
        Set result = TypeUtils.getNominalSet(Long.class);
        Assert.assertNull(result);
    }

    /**
     * Test of getNominalSet method, of class TypeUtils.
     */
    public void testGetNominalSet10() {
        Set result = TypeUtils.getNominalSet(short.class);
        Assert.assertNull(result);
    }

    /**
     * Test of getNominalSet method, of class TypeUtils.
     */
    public void testGetNominalSet11() {
        Set result = TypeUtils.getNominalSet(Short.class);
        Assert.assertNull(result);
    }

    /**
     * Test of getNominalSet method, of class TypeUtils.
     */
    public void testGetNominalSet12() {
        Set result = TypeUtils.getNominalSet(int[].class);
        Assert.assertNull(result);
    }

    /**
     * Test of getNominalSet method, of class TypeUtils.
     */
    public void testGetNominalSet13() {
        Set result = TypeUtils.getNominalSet(TypeUtilsTest.class);
        Assert.assertNull(result);
    }

    /**
     * Test of getNominalSet method, of class TypeUtils.
     */
    public void testGetNominalSet14() {
        Set result = TypeUtils.getNominalSet(double.class);
        Assert.assertNull(result);
    }

    /**
     * Test of getNominalSet method, of class TypeUtils.
     */
    public void testGetNominalSet15() {
        Set result = TypeUtils.getNominalSet(Double.class);
        Assert.assertNull(result);
    }

    /**
     * Test of getNominalSet method, of class TypeUtils.
     */
    public void testGetNominalSet16() {
        Set result = TypeUtils.getNominalSet(float.class);
        Assert.assertNull(result);
    }

    /**
     * Test of getNominalSet method, of class TypeUtils.
     */
    public void testGetNominalSet17() {
        Set result = TypeUtils.getNominalSet(Float.class);
        Assert.assertNull(result);
    }

    /**
     * Test of getNominalSet method, of class TypeUtils.
     */
    @SuppressWarnings("unchecked")
    public void testGetNominalSet18() {
        Set result = TypeUtils.getNominalSet(FooEnum.class);
        Assert.assertNotNull(result);
        Assert.assertEquals(FooEnum.values().length, result.size());
        AssertExtensions.assertContains(FooEnum.Foo, result);
        AssertExtensions.assertContains(FooEnum.Bar, result);
        AssertExtensions.assertContains(FooEnum.Foobar, result);
        AssertExtensions.assertContains(FooEnum.Fubar, result);
        AssertExtensions.assertContains(FooEnum.Baz, result);
        AssertExtensions.assertContains(FooEnum.Qux, result);
        AssertExtensions.assertContains(FooEnum.Quux, result);
    }

    /**
     * Test of getNominalSet method, of class TypeUtils.
     */
    public void testGetNominalSet19() {
        Set result = TypeUtils.getNominalSet(Byte.class);
        Assert.assertNotNull(result);
        Assert.assertEquals(ByteSet.Instance, result);
    }

    /**
     * Test of getNominalSet method, of class TypeUtils.
     */
    public void testGetNominalSet20() {
        Set result = TypeUtils.getNominalSet(byte.class);
        Assert.assertNotNull(result);
        Assert.assertEquals(ByteSet.Instance, result);
    }

    //null, bool, char, string, object, int, long, short, int[], TypeUtilsTest, double, float, FooEnum, byte
}
