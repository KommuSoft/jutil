package jutils.collections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Objects;
import junit.framework.Assert;
import junit.framework.TestCase;
import jutils.iterators.SingleIterable;
import jutils.iterators.SingleIterator;
import jutils.testing.AssertExtensions;
import jutils.tuples.Tuple2Base;

/**
 *
 * @author kommusoft
 */
public class CollectionUtilsTest extends TestCase {

    public CollectionUtilsTest() {
    }

    /**
     * Test of setAllValues method, of class CollectionUtils.
     */
    public void testSetAllValues00() {
        Map<Object, Object> expResult = null;
        Map<Object, Object> result = null;
        Map<Object, Object> result2 = CollectionUtils.setAllValues(result, 3);
        AssertExtensions.assertEquals(result, result2);
        AssertExtensions.assertEquals(expResult, result);
    }

    /**
     * Test of setAllValues method, of class CollectionUtils.
     */
    public void testSetAllValues01() {
        Map<Object, Object> expResult = new HashMap<>();
        Map<Object, Object> result = new HashMap<>();
        Map<Object, Object> result2 = CollectionUtils.setAllValues(result, 3);
        AssertExtensions.assertEquals(result, result2);
        AssertExtensions.assertEquals(expResult, result);
    }

    /**
     * Test of setAllValues method, of class CollectionUtils.
     */
    public void testSetAllValues02() {
        Map<Object, Object> expResult = new HashMap<>();
        expResult.put("a", "b");
        Map<Object, Object> result = new HashMap<>();
        result.put("a", "a");
        Map<Object, Object> result2 = CollectionUtils.setAllValues(result, "b");
        AssertExtensions.assertEquals(result, result2);
        AssertExtensions.assertEquals(expResult, result);
    }

    /**
     * Test of setAllValues method, of class CollectionUtils.
     */
    public void testSetAllValues03() {
        Map<Object, Object> expResult = new HashMap<>();
        expResult.put("a", "b");
        expResult.put("b", "b");
        Map<Object, Object> result = new HashMap<>();
        result.put("a", "a");
        result.put("b", "b");
        Map<Object, Object> result2 = CollectionUtils.setAllValues(result, "b");
        AssertExtensions.assertEquals(result, result2);
        AssertExtensions.assertEquals(expResult, result);
    }

    /**
     * Test of setAllValues method, of class CollectionUtils.
     */
    public void testSetAllValues04() {
        Map<Object, Object> expResult = new HashMap<>();
        expResult.put("a", 4);
        expResult.put("b", 4);
        Map<Object, Object> result = new HashMap<>();
        result.put("a", "a");
        result.put("b", "b");
        Map<Object, Object> result2 = CollectionUtils.setAllValues(result, 4);
        AssertExtensions.assertEquals(result, result2);
        AssertExtensions.assertEquals(expResult, result);
    }

    /**
     * Test of setAllValues method, of class CollectionUtils.
     */
    public void testSetAllValues05() {
        Map<Object, Object> expResult = new HashMap<>();
        expResult.put(0.07d, 4);
        expResult.put("b", 4);
        Map<Object, Object> result = new HashMap<>();
        result.put("b", "a");
        result.put(0.07d, "b");
        Map<Object, Object> result2 = CollectionUtils.setAllValues(result, 4);
        AssertExtensions.assertEquals(result, result2);
        AssertExtensions.assertEquals(expResult, result);
    }

    /**
     * Test of setAllValues method, of class CollectionUtils.
     */
    public void testSetAllValues06() {
        Map<Object, Object> expResult = new HashMap<>();
        expResult.put(0.07d, null);
        expResult.put("b", null);
        Map<Object, Object> result = new HashMap<>();
        result.put("b", "a");
        result.put(0.07d, "b");
        Map<Object, Object> result2 = CollectionUtils.setAllValues(result, null);
        AssertExtensions.assertEquals(result, result2);
        AssertExtensions.assertEquals(expResult, result);
    }

    /**
     * Test of deepToString method, of class CollectionUtils.
     */
    public void testDeepToString00() {
        Object val = null;
        String expected = Objects.toString(val);
        String result = CollectionUtils.deepToString(val);
        Assert.assertEquals(expected, result);
    }

    /**
     * Test of deepToString method, of class CollectionUtils.
     */
    public void testDeepToString01() {
        Object val = 4.0d;
        String expected = Objects.toString(val);
        String result = CollectionUtils.deepToString(val);
        Assert.assertEquals(expected, result);
    }

    /**
     * Test of deepToString method, of class CollectionUtils.
     */
    public void testDeepToString02() {
        Object val = "Foo";
        String expected = Objects.toString(val);
        String result = CollectionUtils.deepToString(val);
        Assert.assertEquals(expected, result);
    }

    /**
     * Test of deepToString method, of class CollectionUtils.
     */
    public void testDeepToString03() {
        Tuple2Base<String, Integer> val = new Tuple2Base<>("Bar", 0x02);
        String expected = Objects.toString(val);
        String result = CollectionUtils.deepToString(val);
        Assert.assertEquals(expected, result);
    }

    /**
     * Test of deepToString method, of class CollectionUtils.
     */
    public void testDeepToString04() {
        Tuple2Base<String, Integer> item1 = new Tuple2Base<>("Bar", 0x02);
        Iterable<?> val = new SingleIterable<>(item1);
        String expected = String.format("[ %s ]", Objects.toString(item1));
        String result = CollectionUtils.deepToString(val);
        Assert.assertEquals(expected, result);
    }

    /**
     * Test of deepToString method, of class CollectionUtils.
     */
    public void testDeepToString05() {
        Tuple2Base<String, Integer> item1 = new Tuple2Base<>("Bar", 0x02);
        Iterator<?> val = new SingleIterator<>(item1);
        String expected = String.format("[ %s ]", Objects.toString(item1));
        String result = CollectionUtils.deepToString(val);
        Assert.assertEquals(expected, result);
    }

    /**
     * Test of deepToString method, of class CollectionUtils.
     */
    public void testDeepToString06() {
        Tuple2Base<String, Integer> item1 = new Tuple2Base<>("Bar", 0x02);
        Iterator<?> val = new SingleIterator<>(new SingleIterable<>(item1));
        String expected = String.format("[ [ %s ] ]", Objects.toString(item1));
        String result = CollectionUtils.deepToString(val);
        Assert.assertEquals(expected, result);
    }

    /**
     * Test of deepToString method, of class CollectionUtils.
     */
    public void testDeepToString07() {
        Tuple2Base<String, Integer> item1 = new Tuple2Base<>("Bar", 0x02);
        Iterable<?> val = new SingleIterable<>(new SingleIterator<>(item1));
        String expected = String.format("[ [ %s ] ]", Objects.toString(item1));
        String result = CollectionUtils.deepToString(val);
        Assert.assertEquals(expected, result);
    }

    /**
     * Test of deepToString method, of class CollectionUtils.
     */
    public void testDeepToString08() {
        Iterable<?> val = new ArrayList<>();
        String expected = "[  ]";
        String result = CollectionUtils.deepToString(val);
        Assert.assertEquals(expected, result);
    }

    /**
     * Test of deepToString method, of class CollectionUtils.
     */
    public void testDeepToString09() {
        Object item1 = 0x04;
        Object item2 = 5.0d;
        ArrayList<Object> val = new ArrayList<>();
        val.add(item1);
        val.add(item2);
        String expected = String.format("[ %s , %s ]", item1, item2);
        String result = CollectionUtils.deepToString(val);
        Assert.assertEquals(expected, result);
    }

    /**
     * Test of deepToString method, of class CollectionUtils.
     */
    public void testDeepToString10() {
        Object item11 = 0x04;
        Object item12 = 5.0d;
        Object item2 = "Foobar";
        ArrayList<Object> item1 = new ArrayList<>();
        item1.add(item11);
        item1.add(item12);
        LinkedList<Object> val = new LinkedList<>();
        val.add(item1);
        val.add(item2);
        String expected = String.format("[ [ %s , %s ] , %s ]", item11, item12, item2);
        String result = CollectionUtils.deepToString(val);
        Assert.assertEquals(expected, result);
    }

    /**
     * Test of removeAll method, of class CollectionUtils.
     */
    public void testRemoveAll00() {
        Map<Object, Object> expResult = null;
        Map<Object, Object> result = null;
        CollectionUtils.removeAll(result, (Iterable<Object>) null);
        AssertExtensions.assertEquals(expResult, result);
    }

    /**
     * Test of removeAll method, of class CollectionUtils.
     */
    public void testRemoveAll01() {
        Map<Object, Object> expResult = null;
        Map<Object, Object> result = null;
        CollectionUtils.removeAll(result, (Iterable<Object>) null, null);
        AssertExtensions.assertEquals(expResult, result);
    }

    /**
     * Test of removeAll method, of class CollectionUtils.
     */
    public void testRemoveAll02() {
        Map<Object, Object> expResult = null;
        Map<Object, Object> result = null;
        CollectionUtils.removeAll(result, (Iterator<Object>) null);
        AssertExtensions.assertEquals(expResult, result);
    }

    /**
     * Test of removeAll method, of class CollectionUtils.
     */
    public void testRemoveAll03() {
        Map<Object, Object> expResult = null;
        Map<Object, Object> result = null;
        CollectionUtils.removeAll(result, (Iterator<Object>) null, null);
        AssertExtensions.assertEquals(expResult, result);
    }

    /**
     * Test of removeAll method, of class CollectionUtils.
     */
    public void testRemoveAll04() {
        Map<Object, Object> expResult = new HashMap<>();
        expResult.put("a", 4.0d);
        Map<Object, Object> result = new HashMap<>();
        result.put("a", 4.0d);
        CollectionUtils.removeAll(result, (Iterable<Object>) null);
        AssertExtensions.assertEquals(expResult, result);
    }

    /**
     * Test of removeAll method, of class CollectionUtils.
     */
    public void testRemoveAll05() {
        Map<Object, Object> expResult = new HashMap<>();
        expResult.put("a", 4.0d);
        Map<Object, Object> result = new HashMap<>();
        result.put("a", 4.0d);
        CollectionUtils.removeAll(result, (Iterable<Object>) null, null);
        AssertExtensions.assertEquals(expResult, result);
    }

    /**
     * Test of removeAll method, of class CollectionUtils.
     */
    public void testRemoveAll06() {
        Map<Object, Object> expResult = new HashMap<>();
        expResult.put("a", 4.0d);
        Map<Object, Object> result = new HashMap<>();
        result.put("a", 4.0d);
        CollectionUtils.removeAll(result, (Iterator<Object>) null);
        AssertExtensions.assertEquals(expResult, result);
    }

    /**
     * Test of removeAll method, of class CollectionUtils.
     */
    public void testRemoveAll07() {
        Map<Object, Object> expResult = new HashMap<>();
        expResult.put("a", 4.0d);
        Map<Object, Object> result = new HashMap<>();
        result.put("a", 4.0d);
        CollectionUtils.removeAll(result, (Iterator<Object>) null, null);
        AssertExtensions.assertEquals(expResult, result);
    }

    /**
     * Test of removeAll method, of class CollectionUtils.
     */
    public void testRemoveAll08() {
        Map<Object, Object> expResult = new HashMap<>();
        expResult.put("a", 4.0d);
        Map<Object, Object> result = new HashMap<>();
        result.put("a", 4.0d);
        CollectionUtils.removeAll(result, new SingleIterator<>("b"));
        AssertExtensions.assertEquals(expResult, result);
    }

    /**
     * Test of removeAll method, of class CollectionUtils.
     */
    public void testRemoveAll09() {
        Map<Object, Object> expResult = new HashMap<>();
        expResult.put("a", 4.0d);
        Map<Object, Object> result = new HashMap<>();
        result.put("a", 4.0d);
        CollectionUtils.removeAll(result, new SingleIterator<>("b"), null);
        AssertExtensions.assertEquals(expResult, result);
    }

    /**
     * Test of removeAll method, of class CollectionUtils.
     */
    public void testRemoveAll10() {
        Map<Object, Object> expResult = new HashMap<>();
        expResult.put("a", 4.0d);
        Map<Object, Object> result = new HashMap<>();
        result.put("a", 4.0d);
        CollectionUtils.removeAll(result, new SingleIterator<>("b"));
        AssertExtensions.assertEquals(expResult, result);
    }

    /**
     * Test of removeAll method, of class CollectionUtils.
     */
    public void testRemoveAll11() {
        Map<Object, Object> expResult = new HashMap<>();
        expResult.put("a", 4.0d);
        Map<Object, Object> result = new HashMap<>();
        result.put("a", 4.0d);
        CollectionUtils.removeAll(result, new SingleIterator<>("b"), null);
        AssertExtensions.assertEquals(expResult, result);
    }

    /**
     * Test of removeAll method, of class CollectionUtils.
     */
    public void testRemoveAll12() {
        Map<Object, Object> expResult = new HashMap<>();
        Map<Object, Object> result = new HashMap<>();
        result.put("a", 4.0d);
        CollectionUtils.removeAll(result, new SingleIterator<>("a"));
        AssertExtensions.assertEquals(expResult, result);
    }

    /**
     * Test of removeAll method, of class CollectionUtils.
     */
    public void testRemoveAll13() {
        Map<Object, Object> expResult = new HashMap<>();
        Map<Object, Object> result = new HashMap<>();
        result.put("a", 4.0d);
        CollectionUtils.removeAll(result, new SingleIterator<>("a"), null);
        AssertExtensions.assertEquals(expResult, result);
    }

    /**
     * Test of removeAll method, of class CollectionUtils.
     */
    public void testRemoveAll14() {
        Map<Object, Object> expResult = new HashMap<>();
        Map<Object, Object> result = new HashMap<>();
        result.put("a", 4.0d);
        CollectionUtils.removeAll(result, new SingleIterator<>("a"));
        AssertExtensions.assertEquals(expResult, result);
    }

    /**
     * Test of removeAll method, of class CollectionUtils.
     */
    public void testRemoveAll15() {
        Map<Object, Object> expResult = new HashMap<>();
        Map<Object, Object> result = new HashMap<>();
        result.put("a", 4.0d);
        CollectionUtils.removeAll(result, new SingleIterator<>("a"), null);
        AssertExtensions.assertEquals(expResult, result);
    }

    /**
     * Test of removeAll method, of class CollectionUtils.
     */
    public void testRemoveAll16() {
        Map<Object, Object> expResult = new HashMap<>();
        Map<Object, Object> result = new HashMap<>();
        result.put("a", 4.0d);
        LinkedList<Object> vals = new LinkedList<Object>();
        CollectionUtils.removeAll(result, new SingleIterator<>("a"), vals);
        AssertExtensions.assertEquals(expResult, result);
    }

    /**
     * Test of removeAll method, of class CollectionUtils.
     */
    public void testRemoveAll17() {
        Map<Object, Object> expResult = new HashMap<>();
        Map<Object, Object> result = new HashMap<>();
        result.put("a", 4.0d);
        LinkedList<Object> vals = new LinkedList<Object>();
        CollectionUtils.removeAll(result, new SingleIterator<>("a"), vals);
        AssertExtensions.assertEquals(expResult, result);
    }

    /**
     * Test of map method, of class CollectionUtils.
     */
    public void testMap00() {
    }

    /**
     * Test of append method, of class CollectionUtils.
     */
    public void testAppend00() {
    }

    /**
     * Test of argMax method, of class CollectionUtils.
     */
    public void testArgMax_Iterable_Function00() {
    }

    /**
     * Test of argMax method, of class CollectionUtils.
     */
    public void testArgMax_Iterator_Function00() {
    }

    /**
     * Test of argMin method, of class CollectionUtils.
     */
    public void testArgMin_Iterable_Function00() {
    }

    /**
     * Test of argMin method, of class CollectionUtils.
     */
    public void testArgMin_Iterator_Function00() {
    }

    /**
     * Test of addtoList method, of class CollectionUtils.
     */
    public void testAddtoList00() {
    }

    /**
     * Test of classify method, of class CollectionUtils.
     */
    public void testClassify_3args00() {
    }

    /**
     * Test of classify method, of class CollectionUtils.
     */
    public void testClassify_4args00() {
    }

    /**
     * Test of incrementKey method, of class CollectionUtils.
     */
    public void testIncrementKey_Map_GenericType00() {
    }

    /**
     * Test of incrementKey method, of class CollectionUtils.
     */
    public void testIncrementKey_3args00() {
    }

    /**
     * Test of incrementKey method, of class CollectionUtils.
     */
    public void testIncrementKey_4args_1_00() {
    }

    /**
     * Test of incrementKey method, of class CollectionUtils.
     */
    public void testIncrementKey_4args_2_00() {
    }

    /**
     * Test of decrementKey method, of class CollectionUtils.
     */
    public void testDecrementKey_Map_GenericType00() {
    }

    /**
     * Test of decrementKey method, of class CollectionUtils.
     */
    public void testDecrementKey_3args00() {
    }

    /**
     * Test of decrementKey method, of class CollectionUtils.
     */
    public void testDecrementKey_4args00() {
    }

}
